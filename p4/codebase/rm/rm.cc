
#include "rm.h"

#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <string>
#include <typeinfo>

RelationManager* RelationManager::_rm = 0;

RelationManager* RelationManager::instance()
{
    if(!_rm)
        _rm = new RelationManager();

    return _rm;
}

RelationManager::RelationManager()
: tableDescriptor(createTableDescriptor()),
columnDescriptor(createColumnDescriptor()),
indexDescriptor(createIndexDescriptor())
{
}

RelationManager::~RelationManager()
{
}

RC RelationManager::createCatalog()
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    // Create both tables and columns tables, return error if either fails
    RC rc;
    rc = rbfm->createFile(getFileName(TABLES_TABLE_NAME));
    if (rc)
        return rc;
    rc = rbfm->createFile(getFileName(COLUMNS_TABLE_NAME));
    if (rc)
        return rc;
    rc = rbfm->createFile(getFileName(INDEX_TABLE_NAME));
    if (rc)
        return rc;

    // Add table entries for both Tables and Columns
    rc = insertTable(TABLES_TABLE_ID, 1, TABLES_TABLE_NAME);
    if (rc)
        return rc;
    rc = insertTable(COLUMNS_TABLE_ID, 1, COLUMNS_TABLE_NAME);
    if (rc)
        return rc;
    rc = insertTable(INDEX_TABLE_ID, 1, INDEX_TABLE_NAME);
    if (rc)
        return rc;


    // Add entries for tables and columns to Columns table
    rc = insertColumns(TABLES_TABLE_ID, tableDescriptor);
    if (rc)
        return rc;
    rc = insertColumns(COLUMNS_TABLE_ID, columnDescriptor);
    if (rc)
        return rc;
    rc = insertColumns(INDEX_TABLE_ID, indexDescriptor);
    if(rc)
      return(rc);

    return SUCCESS;
}

// Just delete the the two catalog files
RC RelationManager::deleteCatalog()
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();

    RC rc;

    rc = rbfm->destroyFile(getFileName(TABLES_TABLE_NAME));
    if (rc)
        return rc;

    rc = rbfm->destroyFile(getFileName(COLUMNS_TABLE_NAME));
    if (rc)
        return rc;

    rc = rbfm->destroyFile(getFileName(INDEX_TABLE_NAME));
    if (rc)
        return rc;

    return SUCCESS;
}

RC RelationManager::createTable(const string &tableName, const vector<Attribute> &attrs)
{
    RC rc;
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();

    // Create the rbfm file to store the table
    if ((rc = rbfm->createFile(getFileName(tableName)))){
      return(rc);
    }

    // Get the table's ID
    int32_t id;
    rc = getNextTableID(id);
    if (rc){
      return(rc);
    }

    // Insert the table into the Tables table (0 means this is not a system table)
    rc = insertTable(id, 0, tableName);
    if (rc)
        return rc;

    // Insert the table's columns into the Columns table
    rc = insertColumns(id, attrs);
    if (rc)
        return rc;

    return SUCCESS;
}

RC RelationManager::deleteTable(const string &tableName)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    IndexManager *ix = IndexManager::instance();
    RC rc;

    // If this is a system table, we cannot delete it
    bool isSystem;
    rc = isSystemTable(isSystem, tableName);
    if (rc)
        return rc;
    if (isSystem)
        return RM_CANNOT_MOD_SYS_TBL;

    // Delete the rbfm file holding this table's entries
    rc = rbfm->destroyFile(getFileName(tableName));
    if (rc)
        return rc;

    // Grab the table ID
    int32_t id;
    rc = getTableID(tableName, id);
    if (rc)
        return rc;

    // Open tables file
    FileHandle fileHandle;
    rc = rbfm->openFile(getFileName(TABLES_TABLE_NAME), fileHandle);
    if (rc)
        return rc;

    // Find entry with same table ID
    // Use empty projection because we only care about RID
    RBFM_ScanIterator rbfm_si;
    vector<string> projection; // Empty
    void *value = &id;

    rc = rbfm->scan(fileHandle, tableDescriptor, TABLES_COL_TABLE_ID, EQ_OP, value, projection, rbfm_si);

    RID rid;
    rc = rbfm_si.getNextRecord(rid, NULL);
    if (rc)
        return rc;

    // Delete RID from table and close file
    rbfm->deleteRecord(fileHandle, tableDescriptor, rid);
    rbfm->closeFile(fileHandle);
    rbfm_si.close();

    // Delete from Columns table
    rc = rbfm->openFile(getFileName(COLUMNS_TABLE_NAME), fileHandle);
    if (rc)
        return rc;

    // Find all of the entries whose table-id equal this table's ID
    rbfm->scan(fileHandle, columnDescriptor, COLUMNS_COL_TABLE_ID, EQ_OP, value, projection, rbfm_si);

    while((rc = rbfm_si.getNextRecord(rid, NULL)) == SUCCESS)
    {
        // Delete each result with the returned RID
        rc = rbfm->deleteRecord(fileHandle, columnDescriptor, rid);
        if (rc)
            return rc;
    }
    if (rc != RBFM_EOF)
        return rc;

    rbfm->closeFile(fileHandle);
    rbfm_si.close();


    //Delete the indexes associated with the table being deleted

    //Variables here because I am lazy and don't want to scroll all the way up
    FileHandle handle;
    RBFM_ScanIterator itr;
    void *dataBuff = NULL;

    //Open up the file containing the indexes
    rc = rbfm->openFile(INDEX_TABLE_NAME, handle);
    if(rc){
      return(rc);
    }

    //Calloc a buffer large enough to read in an entire index record
    dataBuff = calloc(INDEX_RECORD_DATA_SIZE, 1);
    if(!dataBuff){
      return(-1);
    }

    //Setup the scanner
    rbfm->scan(handle, indexDescriptor, INDEX_COL_TABLE_ID, EQ_OP, value, projection, itr);
    //For each index that has the same table id as the one being deleted
    while((rc = itr.getNextRecord(rid, dataBuff)) == SUCCESS){
      //Delete the record from the index table
      rc = rbfm->deleteRecord(handle, indexDescriptor, rid);
      if(rc){
         free(dataBuff);
         return(rc);
      }

      //And delete the file containing the index tree
      //Assuming that the scanner does not return true on NULL comps,
      //table ID should be non-null, so check to see if file-name is NULL
      if(!(*(char *)(dataBuff) & 0x2)){
         char * temp = ((char *) dataBuff + sizeof(int));
         rc = IndexManager::instance()->destroyFile(string(temp + sizeof(int), *(int *)temp));
         if(rc){
            free(dataBuff);
            return(rc);
         }
      }
    }

    //Close and clean up
    free(dataBuff);
    rbfm->closeFile(handle);
    itr.close();



    return SUCCESS;
}

// Fills the given attribute vector with the recordDescriptor of tableName
RC RelationManager::getAttributes(const string &tableName, vector<Attribute> &attrs)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    // Clear out any old values
    attrs.clear();
    RC rc;

    int32_t id;
    rc = getTableID(tableName, id);
    if (rc)
        return rc;

    void *value = &id;

    // We need to get the three values that make up an Attribute: name, type, length
    // We also need the position of each attribute in the row
    RBFM_ScanIterator rbfm_si;
    vector<string> projection;
    projection.push_back(COLUMNS_COL_COLUMN_NAME);
    projection.push_back(COLUMNS_COL_COLUMN_TYPE);
    projection.push_back(COLUMNS_COL_COLUMN_LENGTH);
    projection.push_back(COLUMNS_COL_COLUMN_POSITION);

    FileHandle fileHandle;
    rc = rbfm->openFile(getFileName(COLUMNS_TABLE_NAME), fileHandle);
    if (rc)
        return rc;

    // Scan through the Column table for all entries whose table-id equals tableName's table id.
    rc = rbfm->scan(fileHandle, columnDescriptor, COLUMNS_COL_TABLE_ID, EQ_OP, value, projection, rbfm_si);
    if (rc)
        return rc;

    RID rid;
    void *data = malloc(COLUMNS_RECORD_DATA_SIZE);

    // IndexedAttr is an attr with a position. The position will be used to sort the vector
    vector<IndexedAttr> iattrs;
    while ((rc = rbfm_si.getNextRecord(rid, data)) == SUCCESS)
    {
        // For each entry, create an IndexedAttr, and fill it with the 4 results
        IndexedAttr attr;
        unsigned offset = 0;

        // For the Columns table, there should never be a null column
        char null;
        memcpy(&null, data, 1);
        if (null)
            rc = RM_NULL_COLUMN;

        // Read in name
        offset = 1;
        int32_t nameLen;
        memcpy(&nameLen, (char*) data + offset, VARCHAR_LENGTH_SIZE);
        offset += VARCHAR_LENGTH_SIZE;
        char name[nameLen + 1];
        name[nameLen] = '\0';
        memcpy(name, (char*) data + offset, nameLen);
        offset += nameLen;
        attr.attr.name = string(name);

        // read in type
        int32_t type;
        memcpy(&type, (char*) data + offset, INT_SIZE);
        offset += INT_SIZE;
        attr.attr.type = (AttrType)type;

        // Read in length
        int32_t length;
        memcpy(&length, (char*) data + offset, INT_SIZE);
        offset += INT_SIZE;
        attr.attr.length = length;

        // Read in position
        int32_t pos;
        memcpy(&pos, (char*) data + offset, INT_SIZE);
        offset += INT_SIZE;
        attr.pos = pos;

        iattrs.push_back(attr);
    }
    // Do cleanup
    rbfm_si.close();
    rbfm->closeFile(fileHandle);
    free(data);
    // If we ended on an error, return that error
    if (rc != RBFM_EOF)
        return rc;

    // Sort attributes by position ascending
    auto comp = [](IndexedAttr first, IndexedAttr second)
        {return first.pos < second.pos;};
    sort(iattrs.begin(), iattrs.end(), comp);

    // Fill up our result with the Attributes in sorted order
    for (auto attr : iattrs)
    {
        attrs.push_back(attr.attr);
    }

    return SUCCESS;
}

RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    RC rc;

    // If this is a system table, we cannot modify it
    bool isSystem;
    rc = isSystemTable(isSystem, tableName);
    if (rc)
        return rc;
    if (isSystem)
        return RM_CANNOT_MOD_SYS_TBL;

    // Get recordDescriptor
    vector<Attribute> recordDescriptor;
    rc = getAttributes(tableName, recordDescriptor);
    if (rc)
        return rc;

    // And get fileHandle
    FileHandle fileHandle;
    rc = rbfm->openFile(getFileName(tableName), fileHandle);
    if (rc)
        return rc;

    // Let rbfm do all the work
    rc = rbfm->insertRecord(fileHandle, recordDescriptor, data, rid);
    rbfm->closeFile(fileHandle);




    //Find all of the indexes impacted by inserting the tuple
    int32_t tableID;
    getTableID(tableName, tableID);
    vector<string> indexColumns = getIndexColumnNames(tableID);
    IXFileHandle iHandle;
    IndexManager *ix = IndexManager::instance();

    //For each column that has an index
    for(auto colName : indexColumns){
      rc = ix->openFile(getFileName("Index." + tableName + "." + colName), iHandle);
      if(rc){
         return(rc);
      }
      int offset = NULL_LEN(recordDescriptor.size());

      //Go through each field in the inserted tuple
      for(int i = 0; i < recordDescriptor.size(); i++){
         //If it is not NULL
         if(!(*((char *)data + i/8) & (0x80 << (i % 8)))){
            //And the field name matches the index name
            if(recordDescriptor.at(i).name == colName){
               //Insert the value into the index tree and break
               rc = ix->insertEntry(iHandle, recordDescriptor.at(i),
                  ((char *)data + offset), rid);
               if(rc){
                  return(rc);
               }
               break;
            }

            //Otherwise increment offset
            if(recordDescriptor.at(i).type == TypeVarChar){
               offset += *(int *)((char *)data + offset);
            }
            offset += sizeof(int);
         }
      }

      //Close the file and get ready to open the next one
      ix->closeFile(iHandle);
    }





    return rc;
}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    RC rc;

    // If this is a system table, we cannot modify it
    bool isSystem;
    rc = isSystemTable(isSystem, tableName);
    if (rc)
        return rc;
    if (isSystem)
        return RM_CANNOT_MOD_SYS_TBL;

    // Get recordDescriptor
    vector<Attribute> recordDescriptor;
    rc = getAttributes(tableName, recordDescriptor);
    if (rc)
        return rc;

    // And get fileHandle
    FileHandle fileHandle;
    rc = rbfm->openFile(getFileName(tableName), fileHandle);
    if (rc)
        return rc;


    //Read in the record here so it can be deleted from index
    void *data = calloc(PAGE_SIZE, 1);
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid, data);
    if(rc){
      return(rc);
    }





    // Let rbfm do all the work
    rc = rbfm->deleteRecord(fileHandle, recordDescriptor, rid);
    rbfm->closeFile(fileHandle);



    //Find all of the indexes impacted by deleting the tuple
    int32_t tableID;
    getTableID(tableName, tableID);
    vector<string> indexColumns = getIndexColumnNames(tableID);
    IXFileHandle iHandle;
    IndexManager *ix = IndexManager::instance();
    RID r;

    //For each column that has an index
    for(auto colName : indexColumns){
      rc = ix->openFile(getFileName("Index." + tableName + "." + colName), iHandle);
      if(rc){
         return(rc);
      }
      int offset = NULL_LEN(recordDescriptor.size());

      //Go through each field in the deleted tuple
      for(int i = 0; i < recordDescriptor.size(); i++){
         //If it is not NULL
         if(!(*((char *)data + i/8) & (0x80 << (i % 8)))){
            //And the field name matches the index name
            if(recordDescriptor.at(i).name == colName){
               //Delete the index value and break
               rc = ix->deleteEntry(iHandle, recordDescriptor.at(i),
                  ((char *)data + offset), r);
               if(rc){
                  return(rc);
               }
               break;
            }

            //Otherwise increment offset
            if(recordDescriptor.at(i).type == TypeVarChar){
               offset += *(int *)((char *)data + offset);
            }
            offset += sizeof(int);
         }
      }

      //Close the file and get ready to open the next one
      ix->closeFile(iHandle);
    }

    return rc;
}

RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    RC rc;

    // If this is a system table, we cannot modify it
    bool isSystem;
    rc = isSystemTable(isSystem, tableName);
    if (rc)
        return rc;
    if (isSystem)
        return RM_CANNOT_MOD_SYS_TBL;

    // Get recordDescriptor
    vector<Attribute> recordDescriptor;
    rc = getAttributes(tableName, recordDescriptor);
    if (rc)
        return rc;

    // And get fileHandle
    FileHandle fileHandle;
    rc = rbfm->openFile(getFileName(tableName), fileHandle);
    if (rc)
        return rc;

    // Let rbfm do all the work
    rc = rbfm->updateRecord(fileHandle, recordDescriptor, data, rid);
    rbfm->closeFile(fileHandle);

    return rc;
}

RC RelationManager::readTuple(const string &tableName, const RID &rid, void *data)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    RC rc;

    // Get record descriptor
    vector<Attribute> recordDescriptor;
    rc = getAttributes(tableName, recordDescriptor);
    if (rc){
      return rc;
    }


    // And get fileHandle
    FileHandle fileHandle;
    rc = rbfm->openFile(getFileName(tableName), fileHandle);
    if (rc){
      return rc;
    }

    // Let rbfm do all the work
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid, data);
    rbfm->closeFile(fileHandle);
    return rc;
}

// Let rbfm do all the work
RC RelationManager::printTuple(const vector<Attribute> &attrs, const void *data)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    return rbfm->printRecord(attrs, data);
}

RC RelationManager::readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    RC rc;

    vector<Attribute> recordDescriptor;
    rc = getAttributes(tableName, recordDescriptor);
    if (rc)
        return rc;

    FileHandle fileHandle;
    rc = rbfm->openFile(getFileName(tableName), fileHandle);
    if (rc)
        return rc;

    rc = rbfm->readAttribute(fileHandle, recordDescriptor, rid, attributeName, data);
    rbfm->closeFile(fileHandle);
    return rc;
}

string RelationManager::getFileName(const char *tableName)
{
    return string(tableName) + string(TABLE_FILE_EXTENSION);
}

string RelationManager::getFileName(const string &tableName)
{
    return tableName + string(TABLE_FILE_EXTENSION);
}

vector<Attribute> RelationManager::createTableDescriptor()
{
    vector<Attribute> td;

    Attribute attr;
    attr.name = TABLES_COL_TABLE_ID;
    attr.type = TypeInt;
    attr.length = (AttrLength)INT_SIZE;
    td.push_back(attr);

    attr.name = TABLES_COL_TABLE_NAME;
    attr.type = TypeVarChar;
    attr.length = (AttrLength)TABLES_COL_TABLE_NAME_SIZE;
    td.push_back(attr);

    attr.name = TABLES_COL_FILE_NAME;
    attr.type = TypeVarChar;
    attr.length = (AttrLength)TABLES_COL_FILE_NAME_SIZE;
    td.push_back(attr);

    attr.name = TABLES_COL_SYSTEM;
    attr.type = TypeInt;
    attr.length = (AttrLength)INT_SIZE;
    td.push_back(attr);

    return td;
}

vector<Attribute> RelationManager::createColumnDescriptor()
{
    vector<Attribute> cd;

    Attribute attr;
    attr.name = COLUMNS_COL_TABLE_ID;
    attr.type = TypeInt;
    attr.length = (AttrLength)INT_SIZE;
    cd.push_back(attr);

    attr.name = COLUMNS_COL_COLUMN_NAME;
    attr.type = TypeVarChar;
    attr.length = (AttrLength)COLUMNS_COL_COLUMN_NAME_SIZE;
    cd.push_back(attr);

    attr.name = COLUMNS_COL_COLUMN_TYPE;
    attr.type = TypeInt;
    attr.length = (AttrLength)INT_SIZE;
    cd.push_back(attr);

    attr.name = COLUMNS_COL_COLUMN_LENGTH;
    attr.type = TypeInt;
    attr.length = (AttrLength)INT_SIZE;
    cd.push_back(attr);

    attr.name = COLUMNS_COL_COLUMN_POSITION;
    attr.type = TypeInt;
    attr.length = (AttrLength)INT_SIZE;
    cd.push_back(attr);

    return cd;
}

vector<Attribute> RelationManager::createIndexDescriptor(){
   vector<Attribute> id;
   Attribute attr;

   attr.name = INDEX_COL_TABLE_ID;
   attr.type = TypeInt;
   attr.length = (AttrLength)INT_SIZE;
   id.push_back(attr);

   attr.name = INDEX_COL_FILE_NAME;
   attr.type = TypeVarChar;
   attr.length = TABLES_COL_FILE_NAME_SIZE;
   id.push_back(attr);

   attr.name = INDEX_COL_COLUMN_NAME;
   attr.type = TypeVarChar;
   attr.length = COLUMNS_COL_COLUMN_NAME_SIZE;
   id.push_back(attr);

   return(id);
}

// Creates the Tables table entry for the given id and tableName
// Assumes fileName is just tableName + file extension
void RelationManager::prepareTablesRecordData(int32_t id, bool system, const string &tableName, void *data)
{
    unsigned offset = 0;

    int32_t name_len = tableName.length();

    string table_file_name = getFileName(tableName);
    int32_t file_name_len = table_file_name.length();

    int32_t is_system = system ? 1 : 0;

    // All fields non-null
    char null = 0;
    // Copy in null indicator
    memcpy((char*) data + offset, &null, 1);
    offset += 1;
    // Copy in table id
    memcpy((char*) data + offset, &id, INT_SIZE);
    offset += INT_SIZE;
    // Copy in varchar table name
    memcpy((char*) data + offset, &name_len, VARCHAR_LENGTH_SIZE);
    offset += VARCHAR_LENGTH_SIZE;
    memcpy((char*) data + offset, tableName.c_str(), name_len);
    offset += name_len;
    // Copy in varchar file name
    memcpy((char*) data + offset, &file_name_len, VARCHAR_LENGTH_SIZE);
    offset += VARCHAR_LENGTH_SIZE;
    memcpy((char*) data + offset, table_file_name.c_str(), file_name_len);
    offset += file_name_len;
    // Copy in system indicator
    memcpy((char*) data + offset, &is_system, INT_SIZE);
    offset += INT_SIZE; // not necessary because we return here, but what if we didn't?
}

// Prepares the Columns table entry for the given id and attribute list
void RelationManager::prepareColumnsRecordData(int32_t id, int32_t pos, Attribute attr, void *data)
{
    unsigned offset = 0;
    int32_t name_len = attr.name.length();

    // None will ever be null
    char null = 0;

    memcpy((char*) data + offset, &null, 1);
    offset += 1;

    memcpy((char*) data + offset, &id, INT_SIZE);
    offset += INT_SIZE;

    memcpy((char*) data + offset, &name_len, VARCHAR_LENGTH_SIZE);
    offset += VARCHAR_LENGTH_SIZE;
    memcpy((char*) data + offset, attr.name.c_str(), name_len);
    offset += name_len;

    int32_t type = attr.type;
    memcpy((char*) data + offset, &type, INT_SIZE);
    offset += INT_SIZE;

    int32_t len = attr.length;
    memcpy((char*) data + offset, &len, INT_SIZE);
    offset += INT_SIZE;

    memcpy((char*) data + offset, &pos, INT_SIZE);
    offset += INT_SIZE;
}

// Insert the given columns into the Columns table
RC RelationManager::insertColumns(int32_t id, const vector<Attribute> &recordDescriptor)
{
    RC rc;

    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();

    FileHandle fileHandle;
    rc = rbfm->openFile(getFileName(COLUMNS_TABLE_NAME), fileHandle);
    if (rc)
        return rc;

    void *columnData = malloc(COLUMNS_RECORD_DATA_SIZE);
    RID rid;
    for (unsigned i = 0; i < recordDescriptor.size(); i++)
    {
        int32_t pos = i+1;
        prepareColumnsRecordData(id, pos, recordDescriptor[i], columnData);
        rc = rbfm->insertRecord(fileHandle, columnDescriptor, columnData, rid);
        if (rc)
            return rc;
    }

    rbfm->closeFile(fileHandle);
    free(columnData);
    return SUCCESS;
}

RC RelationManager::insertTable(int32_t id, int32_t system, const string &tableName)
{
    FileHandle fileHandle;
    RID rid;
    RC rc;
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();

    rc = rbfm->openFile(getFileName(TABLES_TABLE_NAME), fileHandle);
    if (rc)
        return rc;

    void *tableData = malloc (TABLES_RECORD_DATA_SIZE);
    prepareTablesRecordData(id, system, tableName, tableData);
    rc = rbfm->insertRecord(fileHandle, tableDescriptor, tableData, rid);

    rbfm->closeFile(fileHandle);
    free (tableData);
    return rc;
}

// Get the next table ID for creating a table
RC RelationManager::getNextTableID(int32_t &table_id)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    FileHandle fileHandle;
    RC rc;

    rc = rbfm->openFile(getFileName(TABLES_TABLE_NAME), fileHandle);
    if (rc){
      return(rc);
    }

    // Grab only the table ID
    vector<string> projection;
    projection.push_back(TABLES_COL_TABLE_ID);

    // Scan through all tables to get largest ID value
    RBFM_ScanIterator rbfm_si;
    rc = rbfm->scan(fileHandle, tableDescriptor, TABLES_COL_TABLE_ID, NO_OP, NULL, projection, rbfm_si);

    RID rid;
    void *data = malloc (1 + INT_SIZE);
    int32_t max_table_id = 0;
    while ((rc = rbfm_si.getNextRecord(rid, data)) == (SUCCESS))
    {
        // Parse out the table id, compare it with the current max
        int32_t tid;
        fromAPI(tid, data);
        if (tid > max_table_id)
            max_table_id = tid;
    }
    // If we ended on eof, then we were successful
    if (rc == RM_EOF)
        rc = SUCCESS;

    free(data);
    // Next table ID is 1 more than largest table id
    table_id = max_table_id + 1;
    rbfm->closeFile(fileHandle);
    rbfm_si.close();
    return SUCCESS;
}

// Gets the table ID of the given tableName
RC RelationManager::getTableID(const string &tableName, int32_t &tableID)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    FileHandle fileHandle;
    RC rc;

    rc = rbfm->openFile(getFileName(TABLES_TABLE_NAME), fileHandle);
    if (rc)
        return rc;

    // We only care about the table ID
    vector<string> projection;
    projection.push_back(TABLES_COL_TABLE_ID);

    // Fill value with the string tablename in api format (without null indicator)
    void *value = malloc(4 + TABLES_COL_TABLE_NAME_SIZE);
    int32_t name_len = tableName.length();
    memcpy(value, &name_len, INT_SIZE);
    memcpy((char*)value + INT_SIZE, tableName.c_str(), name_len);

    // Find the table entries whose table-name field matches tableName
    RBFM_ScanIterator rbfm_si;
    rc = rbfm->scan(fileHandle, tableDescriptor, TABLES_COL_TABLE_NAME, EQ_OP, value, projection, rbfm_si);

    // There will only be one such entry, so we use if rather than while
    RID rid;
    void *data = malloc (1 + INT_SIZE);
    if ((rc = rbfm_si.getNextRecord(rid, data)) == SUCCESS)
    {
        int32_t tid;
        fromAPI(tid, data);
        tableID = tid;
    }

    free(data);
    free(value);
    rbfm->closeFile(fileHandle);
    rbfm_si.close();
    return rc;
}

// Determine if table tableName is a system table. Set the boolean argument as the result
RC RelationManager::isSystemTable(bool &system, const string &tableName)
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    FileHandle fileHandle;
    RC rc;

    rc = rbfm->openFile(getFileName(TABLES_TABLE_NAME), fileHandle);
    if (rc)
        return rc;

    // We only care about system column
    vector<string> projection;
    projection.push_back(TABLES_COL_SYSTEM);

    // Set up value to be tableName in API format (without null indicator)
    void *value = malloc(5 + TABLES_COL_TABLE_NAME_SIZE);
    int32_t name_len = tableName.length();
    memcpy(value, &name_len, INT_SIZE);
    memcpy((char*)value + INT_SIZE, tableName.c_str(), name_len);

    // Find table whose table-name is equal to tableName
    RBFM_ScanIterator rbfm_si;
    rc = rbfm->scan(fileHandle, tableDescriptor, TABLES_COL_TABLE_NAME, EQ_OP, value, projection, rbfm_si);

    RID rid;
    void *data = malloc (1 + INT_SIZE);
    if ((rc = rbfm_si.getNextRecord(rid, data)) == SUCCESS)
    {
        // Parse the system field from that table entry
        int32_t tmp;
        fromAPI(tmp, data);
        system = tmp == 1;
    }
    if (rc == RBFM_EOF)
        rc = SUCCESS;

    free(data);
    free(value);
    rbfm->closeFile(fileHandle);
    rbfm_si.close();
    return rc;
}

void RelationManager::toAPI(const string &str, void *data)
{
    int32_t len = str.length();
    char null = 0;

    memcpy(data, &null, 1);
    memcpy((char*) data + 1, &len, INT_SIZE);
    memcpy((char*) data + 1 + INT_SIZE, str.c_str(), len);
}

void RelationManager::toAPI(const int32_t integer, void *data)
{
    char null = 0;

    memcpy(data, &null, 1);
    memcpy((char*) data + 1, &integer, INT_SIZE);
}

void RelationManager::toAPI(const float real, void *data)
{
    char null = 0;

    memcpy(data, &null, 1);
    memcpy((char*) data + 1, &real, REAL_SIZE);
}

void RelationManager::fromAPI(string &str, void *data)
{
    char null = 0;
    int32_t len;

    memcpy(&null, data, 1);
    if (null)
        return;

    memcpy(&len, (char*) data + 1, INT_SIZE);

    char tmp[len + 1];
    tmp[len] = '\0';
    memcpy(tmp, (char*) data + 5, len);

    str = string(tmp);
}

void RelationManager::fromAPI(int32_t &integer, void *data)
{
    char null = 0;

    memcpy(&null, data, 1);
    if (null)
        return;

    int32_t tmp;
    memcpy(&tmp, (char*) data + 1, INT_SIZE);

    integer = tmp;
}

void RelationManager::fromAPI(float &real, void *data)
{
    char null = 0;

    memcpy(&null, data, 1);
    if (null)
        return;

    float tmp;
    memcpy(&tmp, (char*) data + 1, REAL_SIZE);

    real = tmp;
}


// This method creates an index on a given attribute of a given table.
// It should also enter existence of the index in the Indexes catalog.
RC RelationManager::createIndex(const string &tableName, const string &attributeName)
{
    RC rc;
    RID rid;
    FileHandle fileHandle;
    unsigned offset = 0;
    IndexManager *ixm = IndexManager::instance();
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();

    //Checking to see if the attributeName exists.
    vector<Attribute> listOfAttributes;
    rc = getAttributes(tableName, listOfAttributes);
    if(rc)
        return rc;

    //Check the list of attributes of the table name we were given so we see if the attribute exists or not.
    while(!listOfAttributes.empty()){
        if(attributeName == listOfAttributes.back().name){
            break;
        } else {
            listOfAttributes.pop_back();
        }
    }

    if(listOfAttributes.empty()){
        //The attribute name does not exist.
        return -1;
    }


    // Create the rbfm file to store the table
    if ((rc = ixm->createFile("Index." + tableName + "."
      + attributeName + ".t")))
        return rc;

    //Get the table ID from the method getTableID.
    int32_t id;
    rc = getTableID(tableName, id);
    if (rc)
        return rc;

    //Get the file name from the getFilename method.
    string fileName = "Index." + tableName + "." + attributeName + ".t";

    //Get the length of the file name
    int32_t file_name_len = fileName.length();

    //get the length of the column  name.
    int32_t column_name_len = attributeName.length();

    //Insert the index I created into the index table.
    rc = rbfm->openFile(getFileName(INDEX_TABLE_NAME), fileHandle);
    if (rc)
        return rc;

    void *tableData = malloc (INDEX_RECORD_DATA_SIZE);

    char null = 0;
    // Copy in null indicator
    memcpy((char*) tableData + offset, &null, 1);
    offset += 1;
    // Copy in table id
    memcpy((char*) tableData + offset, &id, INT_SIZE);
    offset += INT_SIZE;
    // Copy in varchar file name
    memcpy((char*) tableData + offset, &file_name_len, VARCHAR_LENGTH_SIZE);
    offset += VARCHAR_LENGTH_SIZE;
    memcpy((char*) tableData + offset, fileName.c_str(), file_name_len);
    offset += file_name_len;
    // Copy in varchar column name
    memcpy((char*) tableData + offset, &column_name_len, VARCHAR_LENGTH_SIZE);
    offset += VARCHAR_LENGTH_SIZE;
    memcpy((char*) tableData + offset, attributeName.c_str(), file_name_len);
    offset += column_name_len;

    //Insert into our index table.
    rc = rbfm->insertRecord(fileHandle, indexDescriptor, tableData, rid);
    if(rc){
      return(-1);
    }

    rbfm->closeFile(fileHandle);
    free (tableData);


    vector<Attribute> attrs;
    vector<string> proj;
    RBFM_ScanIterator itr;
    tableData = calloc(PAGE_SIZE, 1);
    IndexManager *ix = IndexManager::instance();
    IXFileHandle iHandle;

    getAttributes(tableName, attrs);

    rbfm->openFile(getFileName(tableName), fileHandle);
    rc = ix->openFile(getFileName("Index." + tableName + "." + attributeName), iHandle);
    if(rc){
     return(rc);
    }

    rbfm->scan(fileHandle, attrs, attributeName, NO_OP, NULL, proj, itr);
    while(itr.getNextRecord(rid, tableData) == 0){
      rbfm->readRecord(fileHandle, attrs, rid, tableData);

      offset = NULL_LEN(attrs.size());

      //Go through each field in the inserted tuple
      for(int i = 0; i < attrs.size(); i++){
        //If it is not NULL
        if(!(*((char *)tableData + i/8) & (0x80 << (i % 8)))){
            //And the field name matches the index name
            if(attrs.at(i).name == attributeName){
               //Insert the value into the index tree and break
               rc = ix->insertEntry(iHandle, attrs.at(i),
                  ((char *)tableData + offset), rid);
               if(rc){
                  return(rc);
               }
               break;
            }

            //Otherwise increment offset
            if(attrs.at(i).type == TypeVarChar){
               offset += *(int *)((char *)tableData + offset);
            }
            offset += sizeof(int);
        }
      }
    }





    return(0);
}


// This method destroys an index on a given attribute of a given table.
// It should also reflect the non- existence of that index in the Indexes catalog.
RC RelationManager::destroyIndex(const string &tableName, const string &attributeName)
{
    RC rc;
    IndexManager *ixm = IndexManager::instance();
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();


    //Checking to see if the attributeName exists.
    vector<Attribute> listOfAttributes;
    rc = getAttributes(tableName, listOfAttributes);
    if(rc)
        return rc;

    //Check the list of attributes of the table name we were given so we see if the attribute exists or not.
    while(!listOfAttributes.empty()){
        if(attributeName == listOfAttributes.back().name){
            break;
        } else {
            listOfAttributes.pop_back();
        }
    }

    if(listOfAttributes.empty()){
        //The attribute name does not exist.
        return -1;
    }

    if ((rc = ixm->destroyFile("Index." + tableName + "."
      + attributeName + ".t")))
        return rc;

    int32_t id;
    rc = getTableID(tableName, id);
    if (rc)
        return rc;


    // Open Index file
    FileHandle fileHandle;
    rc = rbfm->openFile(getFileName(INDEX_TABLE_NAME), fileHandle);
    if (rc)
        return rc;


    // Use empty projection because we only care about RID
    RBFM_ScanIterator rbfm_si;
    vector<string> projection; // Empty
    void *value = &id;

    rc = rbfm->scan(fileHandle, indexDescriptor, INDEX_COL_TABLE_ID, EQ_OP, value, projection, rbfm_si);

    RID rid;
    rc = rbfm_si.getNextRecord(rid, NULL);
    if (rc)
        return rc;

    // Delete RID from table and close file
    rbfm->deleteRecord(fileHandle, indexDescriptor, rid);
    rbfm->closeFile(fileHandle);
    rbfm_si.close();
    //free the value pointer.
    free(value);


    return 0;
}


// This method should initialize a condition-based scan over the entries in the open
//index on the given attribute of the given table.
// If the scan initiation method is successful, a RM_IndexScanIterator object called rm_IndexScanIterator is returned.
// (Please see the RM_IndexScanIterator class below.)
// Once underway, by calling RM_IndexScanIterator::getNextEntry(),
// the iterator should produce the entries of all records whose indexed attribute
// key falls into the range specified by the lowKey, highKey, and inclusive flags.
// If lowKey is NULL, it can be interpreted as -infinity. If highKey is NULL, it can be interpreted as +infinity.
// The format of the parameter lowKey and highKey is the same as the format of the key in IndexManager::insertEntry().
RC RelationManager::indexScan(const string &tableName,
                      const string &attributeName,
                      const void *lowKey,
                      const void *highKey,
                      bool lowKeyInclusive,
                      bool highKeyInclusive,
                      RM_IndexScanIterator &rm_IndexScanIterator)
{
    Attribute attr;
    IndexManager *ixm = IndexManager::instance();
    RC rc = ixm->openFile(getFileName("Index." + tableName + "." + attributeName),
      rm_IndexScanIterator.ixfileHandle);
    if(rc){
      return rc;
    }


    vector<Attribute> listOfAttributes;
    rc = getAttributes(tableName, listOfAttributes);
    if(rc)
        return rc;

    while(!listOfAttributes.empty()){
        if(attributeName == listOfAttributes.back().name){
            attr = listOfAttributes.back();
            break;
        } else {
            listOfAttributes.pop_back();
        }
    }

    rc = ixm->scan(rm_IndexScanIterator.ixfileHandle, attr, lowKey,
                     highKey, lowKeyInclusive, highKeyInclusive, rm_IndexScanIterator.ix_iter);
    if (rc)
        return rc;

    return SUCCESS;
}

//Extra creadit from PA2, just here so that the makefile runs correctly.
RC RelationManager::addAttribute(const string &tableName, const Attribute &attr){
    return -1;
}

 RC RelationManager::dropAttribute(const string &tableName, const string &attributeName){
    return -1;
 }


// RM_ScanIterator ///////////////

// Makes use of underlying rbfm_scaniterator
RC RelationManager::scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,
      const void *value,
      const vector<string> &attributeNames,
      RM_ScanIterator &rm_ScanIterator)
{
    // Open the file for the given tableName
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    RC rc = rbfm->openFile(getFileName(tableName), rm_ScanIterator.fileHandle);
    if (rc)
        return rc;

    // grab the record descriptor for the given tableName
    vector<Attribute> recordDescriptor;
    rc = getAttributes(tableName, recordDescriptor);
    if (rc)
        return rc;

    // Use the underlying rbfm_scaniterator to do all the work
    rc = rbfm->scan(rm_ScanIterator.fileHandle, recordDescriptor, conditionAttribute,
                     compOp, value, attributeNames, rm_ScanIterator.rbfm_iter);
    if (rc)
        return rc;

    return SUCCESS;
}

// Let rbfm do all the work
RC RM_ScanIterator::getNextTuple(RID &rid, void *data)
{
    return rbfm_iter.getNextRecord(rid, data);
}

// Close our file handle, rbfm_scaniterator
RC RM_ScanIterator::close()
{
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
    rbfm_iter.close();
    rbfm->closeFile(fileHandle);
    return SUCCESS;
}


// RM_IndexScanIterator ///////////////
// This method should set its output parameters rid and key to be the RID and key, respectively,
// of the next record in the index scan. This method should return RM_EOF if there are no more
// index entries satisfying the scan condition. You may assume that RM component clients will
// not close an index scan while the scan is underway. Hence clients only will close an index
// scans after that scan returns RM_EOF.
RC RM_IndexScanIterator::getNextEntry(RID &rid, void *key)
{
    return ix_iter.getNextEntry(rid, key);
}

// This method should terminate the index scan.
RC RM_IndexScanIterator::close(){
    IndexManager *ixm = IndexManager::instance();
    ix_iter.close();
    ixm->closeFile(ixfileHandle);
    return SUCCESS;
}



















//
vector <string> RelationManager::getIndexColumnNames(int tableID){
   RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
   FileHandle handle;
   RBFM_ScanIterator itr;
   void *dataBuff = NULL;
   vector<string> projection;
   int offset = NULL_LEN(indexDescriptor.size());
   RID rid;
   int retVal;
   vector<string> colNames;

   //Open up the file containing the indexes
   retVal = rbfm->openFile(getFileName(INDEX_TABLE_NAME), handle);
   if(retVal){
      return(colNames);
   }

   //Calloc a buffer large enough to store an index record
   dataBuff = calloc(INDEX_RECORD_DATA_SIZE, 1);
   if(!dataBuff){
      return(colNames);
   }

   //Setup the scanner
   rbfm->scan(handle, indexDescriptor, INDEX_COL_TABLE_ID, EQ_OP, &tableID, projection, itr);
   //For each index that has the same table id as the one being added to
   while((retVal = itr.getNextRecord(rid, NULL)) == SUCCESS){
      retVal = rbfm->readRecord(handle, indexDescriptor, rid, dataBuff);
      if(retVal){
         free(dataBuff);
         return(colNames);
      }

      for(int i = 0; i < indexDescriptor.size(); i++){
         //If field is not NULL
         if(!(*((char *)dataBuff + i/8) & (0x80 << (i % 8)))){
            //And it is the column name field, push the name onto vector
            if(indexDescriptor.at(i).name == INDEX_COL_COLUMN_NAME){
               colNames.push_back(string(((char *)dataBuff + offset + sizeof(int)),
                  *(int *)((char *)dataBuff + offset)));
               break;
            }

            //Otherwise increment offset
            if(indexDescriptor.at(i).type == TypeVarChar){
               offset += *(int *)((char *)dataBuff + offset);
            }
            offset += sizeof(int);
         }
      }
   }



   //Close and clean up
   free(dataBuff);
   rbfm->closeFile(handle);
   itr.close();

   return(colNames);

}
