
#include "qe.h"





/**********************************************************
                        Filter
**********************************************************/
//The filter class handles queries and filters the returns from
//input according to the WHERE clause in the SELECT statement
Filter::Filter(Iterator* input, const Condition &condition) {
   vector<string> splitString;


   //Make sure I am not using NULL pointers
   if(input){
      itr = input;
      itr->getAttributes(attrs);

      //Calculate the absolute maximum length for any data reads
      maxDataLength = NULL_LEN(attrs.size());
      for(auto a : attrs){
         maxDataLength += a.length;
         if(a.type == TypeVarChar){
            maxDataLength += 4;
         }
      }

      //Find the left table name and the left column name
      splitOnPeriod(condition.lhsAttr, splitString);
      if(splitString.size() <= 0){
         cout << "ERROR Filter(): Could not split string" << endl;
      } else{
         if(splitString.at(0) != "Index"){
            lTabName = splitString.at(0);
            lColName = splitString.at(1);
         } else{
            lTabName = splitString.at(1);
            lColName = splitString.at(2);
         }
      }


      //If this is set, that means that the righthand side of the WHERE
      //clause is coming from a table with name rhsAttr
      if(condition.bRhsIsAttr){
         splitOnPeriod(condition.rhsAttr, splitString);
         if(splitString.size() != 2){
            cout << "ERROR Filter(): Could not split string" << endl;
         } else{
            rTabName = splitString.at(0);
            rColName = splitString.at(1);
            RelationManager *rm = RelationManager::instance();
            TableScan ts = TableScan(*rm, rTabName);
            rightItr = &ts;
            rightItr->getAttributes(rightAttrs);

            //Calculate the absolute maximum length for any data reads
            rightMaxDataLength = 0;
            for(auto a : rightAttrs){
               rightMaxDataLength += a.length;
               if(a.type == TypeVarChar){
                  rightMaxDataLength += 4;
               }
            }

         }
      }

   } else{
      cout << "ERROR Filter(): Null iterator argument" << endl;
   }
   cond = condition;
}

/*
* Function: Filter::getNextTuple
* -----------------------------
* Queries the stored iterator and if the query is successful, checks
* the returned data pointer to see if it fits the condition and returns
* it if it does or gets the next if it does not fit
*
* Arguments: data
* data: the buffer to fill if the data is valid
*
* Return: RC
* An integer that indicates successful query (0) or error/EOF (-1)
*/
RC Filter::getNextTuple(void *data){
   void *dataBuff, *rightDataBuff;
   int lOff, rOff;
   Attribute searchAttr;
   string name;
   vector<string> splitString;

   if(!itr){
      cout << "ERROR getNextTuple(): Null iterator" << endl;
      return(-1);
   }

   //Find the attribute associated with the column being searched on
   for(auto a : attrs){
      splitOnPeriod(a.name, splitString);
      name = (splitString.at(0) != "Index") ? splitString.at(1) : splitString.at(2);
      if(name == lColName){
         searchAttr = a;
         break;
      }
   }

   //Callocs
   dataBuff = calloc(maxDataLength, 1);
   if(cond.bRhsIsAttr){
      rightDataBuff = calloc(rightMaxDataLength, 1);
   }

   //Go through each until a tuple that fits the relation is found
   while(itr->getNextTuple(dataBuff) == 0){
      lOff = findFieldOffset(dataBuff, attrs, lColName);
      if(lOff < 0){
         continue;
      }
      //If the right hand side is an attribute
      if(cond.bRhsIsAttr){
         //Set the iterator to the beginning
         P_CAST(rightItr, TableScan, 0)->setIterator();
         //And compare all values until one matches or itr runs out
         while(rightItr->getNextTuple(rightDataBuff) == 0){
            rOff = findFieldOffset(rightDataBuff, rightAttrs, rColName);
            if(rOff < 0){
               continue;
            }

            //If relation is true
            if(compareValues(P_CAST(dataBuff, char, lOff),
               P_CAST(rightDataBuff, char, rOff), cond.op, searchAttr)){

               //Copy in the entire tuple and free buffers
               memcpy(data, dataBuff, maxDataLength);
               free(dataBuff);
               free(rightDataBuff);

               return(0);
            }
         }
      }
      //Otherwise just compare against the stored value
      else{
         if(compareValues(P_CAST(dataBuff, char, lOff),
            cond.rhsValue.data, cond.op, searchAttr)){

            memset(data, 0, maxDataLength);
            memcpy(data, dataBuff, maxDataLength);
            free(dataBuff);

            return(0);
         }
      }
   }

   //Reaching here means that none of the remaining values satisfied relation
   return(-1);
}

/*
* Function: Filter::getAttributes
* -----------------------------
* Returns the attributes of the lefthand iterator
*
* Arguments: attrs
* attrs: The attribute list to fill
*
* Return: None
*/
void Filter::getAttributes(vector<Attribute> &attrs) const{
   itr->getAttributes(attrs);
}









/**********************************************************
                        Project
**********************************************************/
//Project constructor
Project::Project(Iterator *input, const vector<string> &attrNames){

   itr = input;
   itr->getAttributes(baseAttrs);

   maxLength = NULL_LEN(baseAttrs.size());
   for(auto a : baseAttrs){
      for(auto name : attrNames){
         if(a.name == name){
            attrs.push_back(a);
            break;
         }
      }
      maxLength += a.length;
   }
}

/*
* Function: Project::getNextTuple
* -----------------------------
* Queries the stored iterator and if the query is successful, copies
* in only the requested attributes given in the constructor to data.
*
* Arguments: data
* data: the buffer to fill if the data is requested
*
* Return: RC
* An integer that indicates successful query (0) or error/EOF (-1)
*/
RC Project::getNextTuple(void *data){
   void * dataBuff = NULL;
   int offset = NULL_LEN(attrs.size());
   char nullBytes [(int)NULL_LEN(attrs.size())];
   int itrOffset;
   int i = 0;
   int retVal;
   vector<string> splitString;

   //Clear the null byte array
   memset(&nullBytes[0], 0, (int)NULL_LEN(attrs.size()));
   //Calloc a buffer
   dataBuff = calloc(maxLength, 1);
   if(!dataBuff){
      return(-1);
   }

   //Read the next tuple in from the iterator, if -1 then no more


   retVal = itr->getNextTuple(dataBuff);
   if(retVal){
      return(retVal);
   }

   //For each of the requested attributes
   for(auto a : attrs){
      //Find the name of the column
      splitOnPeriod(a.name, splitString);
      //Find the of the column within dataBuff
      itrOffset = findFieldOffset(dataBuff, baseAttrs, splitString.at(1));
      //-2 indicates the field is present but NULL
      if(itrOffset == -2){
         //Set appropriate null bit and go to next attrbute
         nullBytes[i/8] |= (0x80 >> (i % 8));
         continue;
      }
      //Otherwise there was an error
      else if(itrOffset < 0){
         return(-1);
      }

      //Copy in the first 4 bytes, the entire value for int/real and the
      //length for varchars
      memcpy(P_CAST(data, char, offset), P_CAST(dataBuff, char, itrOffset),
         sizeof(int));
      offset += 4;

      //If it is a varchar, copy in the actual characters
      if(a.type == TypeVarChar){
         memcpy(P_CAST(data, char, offset),
            P_CAST(dataBuff, char, itrOffset + sizeof(int)),
            *(int *)P_CAST(dataBuff, char, itrOffset));
         offset += *(int *)P_CAST(dataBuff, char, itrOffset);
      }

      i++;
   }

   //Copy in the null bytes and clean up
   memcpy(data, &nullBytes[0], (int)NULL_LEN(attrs.size()));
   free(dataBuff);

   return(0);
}

/*
* Function: Project::getAttributes
* -----------------------------
* Returns the attribute list of the required attributes
*
* Arguments: attrs
* attrs: The attribute list to fill
*
* Return: None
*/
void Project::getAttributes(vector<Attribute> &attrs) const{
   attrs.clear();

   for(auto a : this->attrs){
      attrs.push_back(a);
   }
}











/**********************************************************
                  Index Nested-Loop Join
**********************************************************/
//Nested loop constructor
INLJoin::INLJoin(Iterator *leftIn, IndexScan *rightIn, const Condition &condition){
   vector<Attribute> temp;
   vector<string> names;

   //Store variables
   itr = leftIn;
   rightItr = rightIn;
   cond = condition;

   //Setup the attribute list for the left iterator
   itr->getAttributes(temp);
   maxLength = NULL_LEN(temp.size());
   for(auto a : temp){
      attrs.push_back(a);
      maxLength += a.length;

      //Find which attribute the condition is looking for and remember
      if(a.name == cond.lhsAttr){
         splitOnPeriod(cond.lhsAttr, names);
         lColName = names.at(1);
         searchAttr = a;
      }
   }

   //Setup the attribtue list for the right iterator
   temp.clear();
   rightItr->getAttributes(temp);
   rightMaxLength = NULL_LEN(temp.size());
   for(auto a : temp){
      rightAttrs.push_back(a);
      rightMaxLength += a.length;

      //Only need to remember the name for righthand, assumes same attr
      if(a.name == cond.rhsAttr){
         splitOnPeriod(cond.rhsAttr, names);
         rColName = names.at(1);
      }
   }





}

/*
* Function: INLJoin::getNextTuple
* -----------------------------
* Checks the left tuple against every right tuple and outputs the next
* combined left and right tuple that fits the condition, does not remove
* duplicate attributes in matches
*
* Arguments: data
* data: the buffer to fill with the combined tuple
*
* Return: RC
* An integer that indicates successful query (0) or error/EOF (-1)
*/
RC INLJoin::getNextTuple(void *data){
   void * dataBuff = NULL;
   void * rightDataBuff = NULL;
   int lOff;
   int rOff;
   int tupleSize;
   int rightTupleSize;
   int nullLength = NULL_LEN(attrs.size());
   int rightNullLength = NULL_LEN(rightAttrs.size());
   int i;

   RelationManager *rm = RelationManager::instance();

   //Calloc data buffers
   dataBuff = calloc(maxLength, 1);
   if(!dataBuff){
      return(-1);
   }

   rightDataBuff = calloc(rightMaxLength, 1);
   if(!rightDataBuff){
      return(-1);
   }

   //Go through each tuple in the left iterator
   while(itr->getNextTuple(dataBuff) == 0){
      //If the field is not there or is null skip this tuple
      lOff = findFieldOffset(dataBuff, attrs, lColName);
      if(lOff < 0){
         continue;
      }

      //Find how long the tuple is exactly, not maximum
      tupleSize = nullLength;
      for(i = 0; i < attrs.size(); i++){
         if(!isFieldNull(dataBuff, i)){
            if(attrs.at(i).type == TypeVarChar){
               tupleSize += *(int *)P_CAST(dataBuff, char, tupleSize);
            }
            tupleSize += sizeof(int);
         }
      }

      //Set the right iterator to the beginning
      rightItr->setIterator(NULL, NULL, 0, 0);
      //Compare each right iterator tuple to the selected left one
      while(rightItr->getNextTuple(rightDataBuff) == 0){
         //If the field is not there or is null skip this tuple
         rOff = findFieldOffset(rightDataBuff, rightAttrs, rColName);
         if(rOff < 0){
            continue;
         }

         //Find how long the tuple is exactly, not maximum
         rightTupleSize = rightNullLength;
         for(i = 0; i < rightAttrs.size(); i++){
            if(!isFieldNull(rightDataBuff, i)){
               if(rightAttrs.at(i).type == TypeVarChar){
                  rightTupleSize += *(int *)P_CAST(rightDataBuff, char,
                     rightTupleSize);
               }
               rightTupleSize += sizeof(int);
            }
         }

         //If the right and left tuples satisfy the condition, copy in
         //this tuple
         if(compareValues(P_CAST(dataBuff, char, lOff),
            P_CAST(rightDataBuff, char, rOff), cond.op, searchAttr)){

            //Find out how many NULL bytes are needed for combined vals
            int offset = NULL_LEN((attrs.size() + rightAttrs.size()));
            memset(data, 0, offset);

            for(int i = 0; i < attrs.size(); i++){
               *(P_CAST(data, char, i/8)) |= (isFieldNull(dataBuff, i) << (7 - (i % 8)));
            }

            for(int i = attrs.size(); i < attrs.size() + rightAttrs.size(); i++){
               *(P_CAST(data, char, i/8)) |= (isFieldNull(rightDataBuff, i) << (7 - (i % 8)));
            }

            //Copy in the first set of data
            memcpy(P_CAST(data, char, offset),
               P_CAST(dataBuff, char, nullLength),
               tupleSize - nullLength);

            //Copy in the second set of data
            offset += tupleSize - nullLength;
            memcpy(P_CAST(data, char, offset),
               P_CAST(rightDataBuff, char, rightNullLength),
               rightTupleSize - rightNullLength);

            free(dataBuff);
            free(rightDataBuff);

            return(0);
         }
      }
   }

   //Otherwise there are no more tuples that match
   return(-1);
}

/*
* Function: INLJoin::getAttributes
* -----------------------------
* Returns the attribute list of the concatenated iterator attribute lists
*
* Arguments: attrs
* attrs: The attribute list to fill
*
* Return: None
*/
void INLJoin::getAttributes(vector<Attribute> &attrs) const{
   attrs.clear();

   //Put all the left attributes first
   for(auto a : this->attrs){
      attrs.push_back(a);
   }

   //Followed by the right attributes
   for(auto a : rightAttrs){
      attrs.push_back(a);
   }

}





















/**********************************************************
                        Helper Functions
**********************************************************/

/*
* Function: findFieldOffset
* -----------------------------
* Searches data for an attribute that has the name fieldName and returns
* the offset to the beginning of that field in data
*
* Arguments: data, attrs, fieldName
* data: the data buffer to find the offset in
* attrs: the attribute list / format of data
* fieldName: the name to search attrs / data for
*
* Return: int
* -2 if the field is NULL or -1 if a column with that name does not exist,
* otherwise returns the offset in bytes from the beginning of data
*/
int findFieldOffset(void * data, vector<Attribute> attrs, string fieldName){
   int offset = NULL_LEN(attrs.size());
   int i = 0;
   vector<string> splitString;
   string name;

   for(auto a : attrs){
      splitOnPeriod(a.name, splitString);
      name = (splitString.at(0) != "Index") ? splitString.at(1) : splitString.at(2);
      if(fieldName == name){
         return((isFieldNull(data, i)) ? -2 : offset);
      }

      if(a.type == TypeVarChar){
         offset += *P_CAST(data, int, offset);
      }
      offset += sizeof(int);

      i++;
   }

   return(-1);
}

/*
* Function: isFieldNull
* -----------------------------
* Checks the NULL bits given in the data buffer for the given index
* and returns if that specified bit is set
*
* Arguments: data
* data: the buffer whose NULL bits are to be checked
* index: the position, starting from 0 of the bit to check
*
* Return: bool
* 1 if the bit is set and 0 if not
*/
bool isFieldNull(void * data, int index){
   return((*P_CAST(data, char, index / 8) & (0x80 >> (index % 8))) == 1);
}

/*
* Function: splitOnPeriod
* -----------------------------
* Splits the given string on the first period encountered, exludes the
* period. The table name should come first followed by column name
*
* Arguments: str, retVec
* str: the string to split
* retVec: the vector that is filled with both parts of the split string
*
* Return: None
*/
void splitOnPeriod(string str, vector<string> &retVec){
   int ind = 0;

   retVec.clear();

   for(int i = 0; i < str.length(); i++){
      if(str[i] == '.'){
         retVec.push_back(string(str, ind, i - ind));
         ind = i;
      }
   }
   retVec.push_back(string(str, ind + 1));
}

/*
* Function: compareValues
* -----------------------------
* Compares the left and right arguments according to the specified
* operation and the associated attribute
*
* Arguments: left, right, op, attr
* left: the left side of the relation
* right: the right side of the relation
* op: the relation operator
* attr: The attribute of both of the sides
*
* Return: bool
* 1 if the relation is true, 0 if the relation is false
*/
bool compareValues(void * left, void * right, CompOp op, Attribute attr){
   switch(op){
      case EQ_OP:
         switch(attr.type){
            case TypeInt:
               return(*P_CAST(left, int, 0) == *P_CAST(right, int, 0));
               break;
            case TypeReal:
               return(*P_CAST(left, float, 0) == *P_CAST(right, float, 0));
               break;
            case TypeVarChar:
               if(*P_CAST(left, int, 0) == *P_CAST(right, int, 0)){
                  return(memcmp(P_CAST(left, char, 4),
                     P_CAST(right, char, 4),
                     *P_CAST(left, int, 0)) == 0);
               } else{
                  return(0);
               }
               break;
         }
         break;
      case LT_OP:
         switch(attr.type){
            case TypeInt:
               return(*P_CAST(left, int, 0) < *P_CAST(right, int, 0));
               break;
            case TypeReal:
               return(*P_CAST(left, float, 0) < *P_CAST(right, float, 0));
               break;
            case TypeVarChar:
               return(memcmp(P_CAST(left, char, 4), P_CAST(right, char, 4),
                  min(*P_CAST(left, int, 0), *P_CAST(right, int, 0))) < 0);
               break;
         }
         break;
      case LE_OP:
         switch(attr.type){
            case TypeInt:
               return(*P_CAST(left, int, 0) <= *P_CAST(right, int, 0));
               break;
            case TypeReal:
               return(*P_CAST(left, float, 0) <= *P_CAST(right, float, 0));
               break;
            case TypeVarChar:
               return(memcmp(P_CAST(left, char, 4), P_CAST(right, char, 4),
                  min(*P_CAST(left, int, 0), *P_CAST(right, int, 0))) <= 0);
               break;
         }
         break;
      case GT_OP:
         switch(attr.type){
            case TypeInt:
               return(*P_CAST(left, int, 0) > *P_CAST(right, int, 0));
               break;
            case TypeReal:
               return(*P_CAST(left, float, 0) > *P_CAST(right, float, 0));
               break;
            case TypeVarChar:
               return(memcmp(P_CAST(left, char, 4), P_CAST(right, char, 4),
                  min(*P_CAST(left, int, 0), *P_CAST(right, int, 0))) > 0);
               break;
         }
         break;
      case GE_OP:
         switch(attr.type){
            case TypeInt:
               return(*P_CAST(left, int, 0) >= *P_CAST(right, int, 0));
               break;
            case TypeReal:
               return(*P_CAST(left, float, 0) >= *P_CAST(right, float, 0));
               break;
            case TypeVarChar:
               return(memcmp(P_CAST(left, char, 4), P_CAST(right, char, 4),
                  min(*P_CAST(left, int, 0), *P_CAST(right, int, 0))) >= 0);
               break;
         }
         break;
      case NE_OP:
         switch(attr.type){
            case TypeInt:
               return(*P_CAST(left, int, 0) != *P_CAST(right, int, 0));
               break;
            case TypeReal:
               return(*P_CAST(left, float, 0) != *P_CAST(right, float, 0));
               break;
            case TypeVarChar:
               return(memcmp(P_CAST(left, char, 4), P_CAST(right, char, 4),
                  min(*P_CAST(left, int, 0), *P_CAST(right, int, 0))) != 0);
               break;
         }
         break;
      case NO_OP:
         return(1);
         break;
   }
}
