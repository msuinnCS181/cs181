#ifndef _ix_h_
#define _ix_h_

#include <vector>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>

#include "../rbf/rbfm.h"

# define IX_EOF (-1)  // end of the index scan

# define DEBUG 0
# define PAGE_DEFAULT 12
# define MIN -13


typedef enum IX_Returns{
   IX_PAGE_NOT_FOUND = MIN,
   IX_NOT_IMPLEMENTED,
   IX_CREATE_FAILED,
   IX_MALLOC_FAILED,
   IX_OPEN_FAILED,
   IX_APPEND_FAILED,
   IX_READ_FAILED,
   IX_DELETE_FAILED,
   IX_HANDLE_IN_USE,
   IX_FILE_DNE,
   IX_NO_DUP_KEY,
   IX_KEY_NOT_FOUND,
   IX_NULL_HANDLE,
   IX_SUCCESS = 0,
}IX_Returns;

class IX_ScanIterator;
class IXFileHandle;

class IndexManager {

    public:
        friend class IX_ScanIterator;
        static IndexManager* instance();

        // Create an index file.
        RC createFile(const string &fileName);

        // Delete an index file.
        RC destroyFile(const string &fileName);

        // Open an index and return an ixfileHandle.
        RC openFile(const string &fileName, IXFileHandle &ixfileHandle);

        // Close an ixfileHandle for an index.
        RC closeFile(IXFileHandle &ixfileHandle);

        // Insert an entry into the given index that is indicated by the given ixfileHandle.
        RC insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        // Delete an entry from the given index that is indicated by the given ixfileHandle.
        RC deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

        // Initialize and IX_ScanIterator to support a range search
        RC scan(IXFileHandle &ixfileHandle,
                const Attribute &attribute,
                const void *lowKey,
                const void *highKey,
                bool lowKeyInclusive,
                bool highKeyInclusive,
                IX_ScanIterator &ix_ScanIterator);

        // Print the B+ tree in pre-order (in a JSON record format)
        void printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute) const;
        void tempPrintBtree(IXFileHandle &ixfileHandle, const Attribute &attribute);

    protected:
        IndexManager();
        ~IndexManager();

    private:
        //Here we create a pointer object to the PagedFileManager class.  This class is inherited from rbfm.h.
        static PagedFileManager *_pf_manager;
        static IndexManager *_index_manager;
        RC parseIXReturn(string funcName, IX_Returns ret);
        void createNewPage(void * page, bool leaf, int left, int right);
        void setTotalSize(void * page, int size);
        int getTotalSize(void * page);
        bool fileExists(const string &fileName);
        void setAsLeaf(void * page, int left, int right);
        void setAsNode(void * page);
        int isLeaf(void * page);
        int traverseTree(const void *val, Attribute attr, IXFileHandle handle,
        vector<int> &pageTrace);
        int compareKeys(const void * first, const void* second, Attribute attr, bool isPair);
        RC insertPair(void * pair, const Attribute &attribute, int entrySize,
        IXFileHandle &ixfileHandle, int leafPage);
        RC insertPairVar(void * pair, const Attribute &attribute, int entrySize,
        IXFileHandle &ixfileHandle, int leafPage);
        int splitLeaf(IXFileHandle handle, int curPageNum,
           Attribute attr, void *pair);
        int insertIntoNode(IXFileHandle handle, void * pair,
           Attribute attr, int nodePage, int pagePtr);
        void helpPrint(IXFileHandle &ixfileHandle, const Attribute &attribute, void* pageData) const;
        int splitNode(IXFileHandle handle, int curPageNum,
           Attribute attr, void *pair, int pagePtr, int parentPtr);
};



class IXFileHandle {
    public:

    // variables to keep counter for each operation
    unsigned ixReadPageCounter;
    unsigned ixWritePageCounter;
    unsigned ixAppendPageCounter;

    // Constructor
    IXFileHandle();

    // Destructor
    ~IXFileHandle();

    // Put the current counter values of associated PF FileHandles into variables
    RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount);

    RC ixReadPage(PageNum pageNum, void *data);
    RC ixWritePage(PageNum pageNum, const void *data);
    RC ixAppendPage(const void *data);
    unsigned getNumberOfPages();

    // Let IndexManager access our private helper methods
    friend class IndexManager;

private:
    FILE *_fd;

    // Private helper methods
    void setfd(FILE *fd);
    FILE *getfd();
};



class IX_ScanIterator {
    public:

        friend class IndexManager;
        // Constructor
        IX_ScanIterator();

        // Destructor
        ~IX_ScanIterator();

        // Get next matching entry
        RC getNextEntry(RID &rid, void *key);

        // Terminate index scan
        RC close();

   private:
      IndexManager* _ix;
      IXFileHandle iHandle;
      int curPage;
      uint32_t curOffset;
      uint32_t totalPages;
      uint32_t totalLength;
      void * pageData;

      Attribute searchAttr;
      const void *lowSearch;
      const void *highSearch;
      bool lowSearchInc;
      bool highSearchInc;

      RC scanInit(IXFileHandle &handle,
             const Attribute &attr,
             const void *lKey,
             const void *hKey,
             bool lKeyInc,
             bool hKeyInc);
};

#endif
