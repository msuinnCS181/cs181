
#include "ix.h"

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <string>

//remove the age_idx to remove things from disk.

IndexManager* IndexManager::_index_manager = NULL;
//PagedFileManager* is telling us we are accessing a PagedFileManager pointer to an object of the class.  Then the next...
//IndexMager:: is the class name and the name of the object pointer we defined in the .h file.
PagedFileManager* IndexManager::_pf_manager = NULL;

string errArg = "";



/***********************************************************************
                           IndexManager
***********************************************************************/

IndexManager* IndexManager::instance()
{
    if(!_index_manager)
        _index_manager = new IndexManager();

    return _index_manager;
}

IndexManager::IndexManager()
{
    // Initialize the internal PagedFileManager instance
    _pf_manager = PagedFileManager::instance();

}

IndexManager::~IndexManager()
{
}


// This method creates a B+ tree index file with the given index name.
RC IndexManager::createFile(const string &fileName)
{
   void *pageData = NULL;
   FileHandle handle;

   //Create a new file
   if(_pf_manager->createFile(fileName.c_str())){
      errArg = fileName;
      return (parseIXReturn("IndexManager::createFile", IX_CREATE_FAILED));
   }

   //Initialize to all zeroes and check if not NULL
   pageData = calloc(PAGE_SIZE, 1);
   if(!pageData){
      return (parseIXReturn("IndexManager::createFile", IX_MALLOC_FAILED));
   }

   //Make a new leaf page since tree is empty
   createNewPage(pageData, 1, 0, 0);

   //Attempt to open the new file
   if (_pf_manager->openFile(fileName.c_str(), handle)){
      free(pageData);
      errArg = fileName;
      return (parseIXReturn("IndexManager::createFile", IX_OPEN_FAILED));
   }

   //Attempt to append the empty page
   if (handle.appendPage(pageData)){
      free(pageData);
      errArg = fileName;
      return (parseIXReturn("IndexManager::createFile", IX_APPEND_FAILED));
   }

   _pf_manager->closeFile(handle);

   free(pageData);

   return (parseIXReturn("IndexManager::createFile", IX_SUCCESS));
}


//This method should delete the index file whose name is fileName.
RC IndexManager::destroyFile(const string &fileName)
{
   if(_pf_manager->destroyFile(fileName)){
      errArg = fileName;
      return (parseIXReturn("IndexManager::destroyFile", IX_DELETE_FAILED));
   }

   return (parseIXReturn("IndexManager::destroyFile", IX_SUCCESS));
}

//This method will open the index file. If the open call is successful,
//an IXFileHandle object for the given index is returned. This
//ixfileHandle is passed to the insertEntry(), deleteEntry(), and scan()
// methods to insert/delete/scan entries in the index file.
RC IndexManager::openFile(const string &fileName, IXFileHandle &ixfileHandle)
{
   // If this handle already has an open file, error
   if (ixfileHandle.getfd() != NULL){
      errArg = fileName;
      return (parseIXReturn("IndexManager::openFile", IX_HANDLE_IN_USE));
   }

   // If the file doesn't exist, error
   if (!fileExists(fileName.c_str())){
      errArg = fileName;
      return (parseIXReturn("IndexManager::openFile", IX_FILE_DNE));
   }

   // Open the file for reading/writing in binary mode
   FILE *pFile;
   pFile = fopen(fileName.c_str(), "rb+");
   // If we fail, error
   if (pFile == NULL){
      errArg = fileName;
      return (parseIXReturn("IndexManager::openFile", IX_OPEN_FAILED));
   }

   ixfileHandle.setfd(pFile);

   return (parseIXReturn("IndexManager::openFile", IX_SUCCESS));
}

//This method closes the index file indicated by the ixfileHandle.
RC IndexManager::closeFile(IXFileHandle &ixfileHandle)
{
   FILE *pFile = ixfileHandle.getfd();

   // If not an open file, error
   if (pFile == NULL){
      return (parseIXReturn("IndexManager::closeFile", IX_FILE_DNE));
   }

   // Flush and close the file
   fclose(pFile);

   ixfileHandle.setfd(NULL);

   return (parseIXReturn("IndexManager::closeFile", IX_SUCCESS));
}

//This method should insert a new entry, a <key,rid> pair, into the
//index file (Approach 2). The second parameter is the attribute
//descriptor of the key, and the parameter rid identifies the record
//that will be paired with the key in the index file. (An index contains
// only the records' ids, not the records themselves.) The format for
//the passed-in key is the following: (1) For INT and REAL: use 4 bytes;
// (2) For VARCHAR: use 4 bytes for the length followed by the
//characters. Note that the given key value doesn't contain Null flags.
//In our system we won't store nulls in the index, so you can safely
//assume all the key data is not null. Note, though, that this rules out
// index-only query plans -- real systems that support them do enter ALL
// records, even the null ones, into their indexes for completeness.
//An overflow can happen if we try to insert an entry into a
//fully-occupied node. As we discussed in the B+ tree part, you are
//required to restructure the tree shape to handle this.
RC IndexManager::insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{
   //Be sure to update the page size after inserting the entry.
   vector <int> path;
   int leafPage;
   int occupiedSpace;
   size_t entrySize;
   void *searchPair = NULL;
   int newEntryPage;

   //Malloc a buffer to read pages into
   void *pageData = calloc(PAGE_SIZE, 1);
   if (pageData == NULL){
      return (parseIXReturn("IndexManager::insertEntry", IX_MALLOC_FAILED));
   }

   //Call traverseTree to get the page we fish to insert on.
   leafPage = traverseTree(key, attribute, ixfileHandle, path);

   //If it is an illegal page number, error out
   if(leafPage < 0 || leafPage >= ixfileHandle.getNumberOfPages()){
      free(pageData);
      return (parseIXReturn("IndexManager::insertEntry",IX_PAGE_NOT_FOUND));
   }

   //Otherwise read in the specified leaf
   if(ixfileHandle.ixReadPage(leafPage, pageData)){
      free(pageData);
      return (parseIXReturn("IndexManager::insertEntry", IX_READ_FAILED));
   }

   //pageData now points to the beginning of the page we wish to insert on.
   //occupiedSpace includes the 8 bytes taken up at the end by isleaf and size indicator.
   occupiedSpace = getTotalSize(pageData);

   //Find out how large the key is
   entrySize = sizeof(int);
   if(attribute.type == TypeVarChar){
      entrySize += *(int *) key;
   }

   //Copy the page into the new buffer
   searchPair = calloc(entrySize + sizeof(RID), 1);
   if(!searchPair){
      free(pageData);
      return (parseIXReturn("IndexManager::insertEntry",IX_MALLOC_FAILED));
   }

   //Allocate buffer space for our the <key,rid> pair for our entry.
   memcpy(searchPair, key, entrySize);
   memcpy(((char *) searchPair + entrySize), &rid, sizeof(RID));

   //Need to split because the page cannot fit the entry we are inserting.
   if((occupiedSpace + entrySize + sizeof(RID)) > PAGE_SIZE){
      //Split the leaf
      newEntryPage = splitLeaf(ixfileHandle, leafPage, attribute, searchPair);
      free(searchPair);

      //Find the size for the key RID pair
      int newEntrySize = sizeof(int) + sizeof(RID);
      if(attribute.type == TypeVarChar){
         newEntrySize += *(int *)key;
      }

      //Special case for splitting root (when root is leaf)
      if(leafPage == 0){
         //This case needs to be handled special since we are splitting the root.
         //Calloc a buffer to read the first entry of the new leaf page
         void *pairBuffer = calloc(newEntrySize, 1);
         if(pairBuffer == NULL){
            //free all callocs up to this point.
            free(pageData);
            return (parseIXReturn("IndexManager::insertEntry",IX_MALLOC_FAILED));
         }

         //Read in the new page
         if(ixfileHandle.ixReadPage(newEntryPage, pageData)){
            free(pageData);
            free(pairBuffer);
            return (parseIXReturn("IndexManager::insertEntry", IX_READ_FAILED));
         }

         //Copy the first value from the page into the buffer
         memcpy(pairBuffer, (char*)pageData + sizeof(int), newEntrySize);

         int insertResult = insertIntoNode(ixfileHandle, pairBuffer,
             attribute, 0, newEntryPage);

         free(pageData);
         free(pairBuffer);

         return parseIXReturn("IndexManager::insertEntry", (IX_Returns) insertResult);
      }
      //Split normally
      else {
         //Get the value of the key that is on the newly created page.
         int parentIndex = path.size()-2;

         //Create a buffer for the parentPage.  It's contents will change on each while loop iteration.
         void *parentBuffer = calloc(PAGE_SIZE, 1);
         if(parentBuffer == NULL){
            //free all callocs up to this point.
            free(pageData);
            return (parseIXReturn("IndexManager::insertEntry",IX_MALLOC_FAILED));
         }

         //Read the next parent into our buffer
         if(ixfileHandle.ixReadPage(path.at(parentIndex), parentBuffer)){
            //free all callocs up to this point.
            free(pageData);
            free(parentBuffer);
            return (parseIXReturn("IndexManager::insertEntry", IX_READ_FAILED));
         }

         //read the next new page into the buffer.
         if(ixfileHandle.ixReadPage(newEntryPage, pageData)){
            free(pageData);
            free(parentBuffer);
            return (parseIXReturn("IndexManager::insertEntry", IX_READ_FAILED));
         }

         //Get the size of the first key,RID pair that is on the newly created page.
         int newEntrySize = sizeof(int) + sizeof(RID);
         if(attribute.type == TypeVarChar){
            newEntrySize += *(int *)((char *)pageData + sizeof(int));
         }

         //Create a buffer to read the entire key RID pair into
         void *pairBuffer = calloc(newEntrySize, 1);
         if(pairBuffer == NULL){
            //free all callocs up to this point.
            free(parentBuffer);
            free(pageData);
            return (parseIXReturn("IndexManager::insertEntry",IX_MALLOC_FAILED));
         }

         //Read the first entry of the new page into the buffer
         memcpy(pairBuffer, (char*)pageData + sizeof(int), newEntrySize);
         int parentSpace = getTotalSize(parentBuffer);

         //If the parent node must be split
         if((parentSpace + newEntrySize) > PAGE_SIZE){
            splitNode(ixfileHandle, path.at(parentIndex),
               attribute, pairBuffer, newEntryPage, 0);
            return((parseIXReturn("IndexManager::insertEntry", IX_SUCCESS)));
         }
         else{
            //The entry fits in this parent node, so we need to insert in the correct spot.
            int insertResult = insertIntoNode(ixfileHandle, pairBuffer,
               attribute, path.at(parentIndex), newEntryPage);

            free(pageData);
            free(parentBuffer);
            free(pairBuffer);
            return parseIXReturn("IndexManager::insertEntry", (IX_Returns) insertResult);
         }
      }
   }
   //There is enough room on the page so we must insert the entry in the correct sorted location.
   else {
      int insertResult;
      if(attribute.type == TypeVarChar){
         cout << "Before insertPairVar" << endl;

         insertResult = insertPairVar(searchPair, attribute,
         entrySize, ixfileHandle, leafPage);

      }else{
         cout << "Before insertPair" << endl;
         insertResult = insertPair(searchPair, attribute,
         entrySize, ixfileHandle, leafPage);
      }

      free(pageData);
      free(searchPair);
      return parseIXReturn("IndexManager::insertEntry", (IX_Returns) insertResult);
   }
}

//This method should delete the entry for the <key,rid> pair from the
//index. You should return an error code if this method is called with a
// non-existing entry. The format of the key is the same as the format
//that is given to insertEntry() function.
RC IndexManager::deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{
   int pageNum;
   vector<int> vec;
   int offset;
   void *pageData = NULL;
   void *searchPair = NULL;
   size_t bytesToCopy;

   //Figure out sizeof pair to delete
   switch(attribute.type){
      case TypeInt:
      case TypeReal:
         bytesToCopy = sizeof(int);
         break;
      case TypeVarChar:
         bytesToCopy = *(int *) key + sizeof(int);
         break;
   }

   //Create a buffer to copy search pair into
   searchPair = calloc(bytesToCopy + sizeof(RID), 1);
   if(!searchPair){
      return (parseIXReturn("IndexManager::deleteEntry",IX_MALLOC_FAILED));
   }

   //Copy the page into the new buffer
   memcpy(searchPair, key, bytesToCopy);
   memcpy(((char *) searchPair + bytesToCopy), &rid, sizeof(RID));

   //Find the page number of the leaf that this pair belongs on
   pageNum = traverseTree(key, attribute, ixfileHandle, vec);

   //If it is an invalid page number
   if(pageNum < 0 || pageNum >= ixfileHandle.getNumberOfPages()){
      free(searchPair);
      return (parseIXReturn("IndexManager::deleteEntry",IX_PAGE_NOT_FOUND));
   }

   //Create a page sized buffer to read data into
   pageData = calloc(PAGE_SIZE, 1);
   if(!pageData){
      free(searchPair);
      return (parseIXReturn("IndexManager::deleteEntry",IX_MALLOC_FAILED));
   }

   //Read in the correct page
   if (ixfileHandle.ixReadPage(pageNum, pageData)){
      errArg = pageNum;
      free(pageData);
      free(searchPair);
      return (parseIXReturn("IndexManager::deleteEntry",IX_READ_FAILED));
   }

   //Check each pair to see if it is the correct one, skip page pointer
   offset = sizeof(int);
   while(offset < getTotalSize(pageData) - PAGE_DEFAULT){
      //Found a match
      if(compareKeys(searchPair, ((char *) pageData + offset), attribute, 1) == 0){

         //Move the following data over
         memmove(((char *)pageData + offset),
            ((char *)pageData + offset + bytesToCopy + sizeof(RID)),
            getTotalSize(pageData) - PAGE_DEFAULT - offset - bytesToCopy
            - sizeof(RID) + sizeof(int));

         //Decrement the size used on the page
         setTotalSize(pageData, getTotalSize(pageData) - bytesToCopy - sizeof(RID));

         //Write the modified page
         ixfileHandle.ixWritePage(pageNum, pageData);

         //Successful delete
         free(pageData);
         free(searchPair);
         return (parseIXReturn("IndexManager::deleteEntry",IX_SUCCESS));
      }

      //Setup and increment the offset
      if(attribute.type == TypeVarChar){
         offset += *(int *)((char *)pageData + offset);
      }
      offset += sizeof(int);
      offset += sizeof(RID);
   }

   //Otherwise, could not find key
   free(pageData);
   free(searchPair);
   return (parseIXReturn("IndexManager::deleteEntry",IX_KEY_NOT_FOUND));
}

//This method should initialize a condition-based scan over the entries
//in the open index. Once underway, by calls to
//IX_ScanIterator::getNextEntry(), the iterator should incrementally
//produce the entries for all records whose indexed attribute key falls
//into the range specified by the lowKey, highKey, and inclusive flags.
//If lowKey is a NULL pointer, it can be interpreted as - infinity. If
//highKey is NULL, it can be interpreted as +infinity. Otherwise the
//format of the parameter lowKey and highKey is the same as the format
//of the key in IndexManager::insertEntry().
RC IndexManager::scan(IXFileHandle &ixfileHandle,
        const Attribute &attribute,
        const void      *lowKey,
        const void      *highKey,
        bool            lowKeyInclusive,
        bool            highKeyInclusive,
        IX_ScanIterator &ix_ScanIterator)
{

   if(ixfileHandle.getfd() == NULL){
      return(parseIXReturn("IndexManager::scan", IX_NULL_HANDLE));
   }

   return (parseIXReturn("IndexManager::scan", (IX_Returns)
      ix_ScanIterator.scanInit(ixfileHandle, attribute,lowKey,highKey,
         lowKeyInclusive,highKeyInclusive)));
}

//This method should print out the B+ tree structure in a pre-order
//fashion. We use it to "eyeball" the shape of your B+ tree in some of
//our test cases. So, it is important to implement this function
//correctly.
//You should print your B+ tree as a valid JSON record to make
//visualization (by you and by our graders) easier. To do so, you will
//need to traverse the B+ tree nodes depth-first to print them in
//pre-order. You should print out all the keys in each node, and also
//its children, in JSON format.
void IndexManager::printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute) const {
   //Current page we are looking at, which is the root.
   int cur = 0;
   //Make an offset to iterate through the page.
   int offset = sizeof(int);
   //Assign a buffer to hold the page we are looking at.
   void *pageData = calloc(PAGE_SIZE, 1);
   //Read the root into the buffer.
   ixfileHandle.ixReadPage(cur, pageData);
   //Starting with the root being a leaf page.
   //Need to check if the root is a reaf node since leaves are printed differently.
   int checkLeaf = (*(int *)((char *) pageData + PAGE_SIZE - 2*sizeof(int)));
   //print out the first formatting symbol.
   cout << "{" << endl;
   if(checkLeaf >= 0){
      //The root is the leaf.
      int entrySize = sizeof(int);
      //Get the total size of space the root is occipying on the page.
      int occupiedSpace = (*(int *)((char *) pageData + PAGE_SIZE - sizeof(int)));
      if(attribute.type == TypeVarChar){
         cout << "Dealing with VarChar!!!" << endl;
         //We are dealing with a varChar file.
         int varLength  = (*(int *)((char *) pageData + offset));
         entrySize += varLength;
         //Has the string data of the newest key we have.
         void * currentKeyBuffer = calloc(PAGE_SIZE, 1);
         void * nextKeyBuffer = calloc(PAGE_SIZE,1);
         memcpy(currentKeyBuffer,(char*)pageData + offset, entrySize);
         cout << "\"keys\": [\"";
         //Alter the offset for a second just to output the key properly.
         offset += sizeof(int);
         for (int i = offset; i < *(int*)((char*) pageData + (offset + varLength)); i++){
             cout << *((char *) pageData + offset + i);
         }
         offset -= sizeof(int);
         cout << ": [";
         while(offset < occupiedSpace - 2*sizeof(int)){
            varLength = (*(int *)((char *) pageData + offset));
            entrySize += varLength;
            memcpy(nextKeyBuffer, (char*)pageData + offset, entrySize);

            //string nextString(((char*) pageData + offset), &varLength);
            if(memcmp(currentKeyBuffer, nextKeyBuffer, entrySize) == 0){
               if(offset > sizeof(int)){
                  cout << ",";
               }
               int currentPage = (*(int *)((char *) pageData + (offset + entrySize)));
               int currentSlot = (*(int *)((char *) pageData + (offset + entrySize + sizeof(int))));
               cout << "(" << currentPage << "," << currentSlot << ")";
               memset(nextKeyBuffer, 0, PAGE_SIZE);
               offset += entrySize + 2*sizeof(int);
            }else{
               //This is a new key we are considerng.
               cout << "]\",\"";
               offset +=sizeof(int);
               for (int i = offset; i < *(int*)((char*) pageData + (offset + varLength)); i++){
                   cout << *((char *) pageData + offset + i);
              }
              offset -= sizeof(int);
              memset(currentKeyBuffer, 0, PAGE_SIZE);
              memcpy(currentKeyBuffer, nextKeyBuffer, entrySize);
              memset(nextKeyBuffer, 0, PAGE_SIZE);
            }
         }
         free(currentKeyBuffer);
         free(nextKeyBuffer);
         free(pageData);
         cout << "]\"]}" <<endl;
      }else{
         //We are dealing with a float or int file.
         int nextKey = (*(int *)((char *) pageData + offset));
         void * currentKeyBuffer = calloc(entrySize, 1);
         memcpy(currentKeyBuffer,(char*)pageData + sizeof(int), entrySize);
         cout << "\"keys\": [\"" << nextKey << ": [";
         while(offset < occupiedSpace - 2*sizeof(int)){
            nextKey = (*(int *)((char *) pageData + offset));
            if(memcmp(currentKeyBuffer, &nextKey, sizeof(int)) == 0){
               if(offset > sizeof(int)){
                  cout << ",";
               }
               //This means that the next key is still the same, so do not print it.
               //Just print it's RID.
               int currentPage = (*(int *)((char *) pageData + (offset + sizeof(int))));
               int currentSlot = (*(int *)((char *) pageData + (offset + 2*sizeof(int))));
               cout << "(" << currentPage << "," << currentSlot << ")";
               offset += entrySize + 2*sizeof(int);
            }else{
               //This is a new key we are considering.
               cout << "]\",\"";
               memset(currentKeyBuffer, 0, entrySize);
               memcpy(currentKeyBuffer, &nextKey, sizeof(int));
               cout << nextKey << ": [";
            }

         }
         cout << "]\"]}" <<endl;
         free(currentKeyBuffer);
      }
      free(pageData);
   }else{
      //The root is not a leaf, so we must call the helpe print function.
      //Begin to do recursive calls
      helpPrint(ixfileHandle, attribute, pageData);
      free(pageData);
   }

}


void IndexManager::helpPrint(IXFileHandle &ixfileHandle, const Attribute &attribute, void* pageData) const {
   cout << "AHHHHHHHHH" << endl;
   //The page in our buffer is the root of our tree and it is not a leaf.  On the first iteration.
    int checkLeaf = (*(int *)((char *) pageData + PAGE_SIZE - 2*sizeof(int)));
    int occupiedSpace = (*(int *)((char *) pageData + PAGE_SIZE - sizeof(int)));
   if(checkLeaf >= 0){
      int entrySize = sizeof(int);
      //Get the total size of space the root is occipying on the page.
      int occupiedSpace = (*(int *)((char *) pageData + PAGE_SIZE - sizeof(int)));
      if(attribute.type == TypeVarChar){
         cout << "in the var case" << endl;
         int offset = sizeof(int);
         cout << "Dealing with VarChar!!!" << endl;
         //We are dealing with a varChar file.
         int varLength  = (*(int *)((char *) pageData + offset));
         entrySize += varLength;
         //Has the string data of the newest key we have.
         //string currentString(((char*) pageData + offset), &varLength);
         void * currentKeyBuffer = calloc(PAGE_SIZE, 1);
         void * nextKeyBuffer = calloc(PAGE_SIZE,1);
         memcpy(currentKeyBuffer,(char*)pageData + offset, entrySize);
         cout << "\"keys\": [\"";
         //Alter the offset for a second just to output the key properly.
         offset += sizeof(int);
         for (int i = offset; i < *(int*)((char*) pageData + (offset + varLength)); i++){
             cout << *((char *) pageData + offset + i);
         }
         offset -= sizeof(int);
         cout << ": [";
         while(offset < occupiedSpace - 2*sizeof(int)){
            varLength = (*(int *)((char *) pageData + offset));
            entrySize += varLength;
            memcpy(nextKeyBuffer, (char*)pageData + offset, entrySize);

            //string nextString(((char*) pageData + offset), &varLength);
            if(memcmp(currentKeyBuffer, nextKeyBuffer, entrySize) == 0){
               if(offset > sizeof(int)){
                  cout << ",";
               }
               int currentPage = (*(int *)((char *) pageData + (offset + entrySize)));
               int currentSlot = (*(int *)((char *) pageData + (offset + entrySize + sizeof(int))));
               cout << "(" << currentPage << "," << currentSlot << ")";
               memset(nextKeyBuffer, 0, PAGE_SIZE);
               offset += entrySize + 2*sizeof(int);
            }else{
               //This is a new key we are considerng.
               cout << "]\",\"";
               offset +=sizeof(int);
               for (int i = offset; i < *(int*)((char*) pageData + (offset + varLength)); i++){
                   cout << *((char *) pageData + offset + i);
              }
              offset -= sizeof(int);
              memset(currentKeyBuffer, 0, PAGE_SIZE);
              memcpy(currentKeyBuffer, nextKeyBuffer, entrySize);
              memset(nextKeyBuffer, 0, PAGE_SIZE);
            }
         }
         free(currentKeyBuffer);
         free(nextKeyBuffer);
         cout << "]\"]}" <<endl;
      }else{
         //We are dealing with a float or int file.
         int offset = sizeof(int);
         int nextKey = (*(int *)((char *) pageData + offset));
         void * currentKeyBuffer = calloc(entrySize, 1);
         memcpy(currentKeyBuffer,(char*)pageData + sizeof(int), entrySize);
         cout << "\"keys\": [\"" << nextKey << ": [";
         while(offset < occupiedSpace - 2*sizeof(int)){
            nextKey = (*(int *)((char *) pageData + offset));
            if(memcmp(currentKeyBuffer, &nextKey, sizeof(int)) == 0){
               if(offset > sizeof(int)){
                  cout << ",";
               }
               //This means that the next key is still the same, so do not print it.
               //Just print it's RID.
               int currentPage = (*(int *)((char *) pageData + (offset + sizeof(int))));
               int currentSlot = (*(int *)((char *) pageData + (offset + 2*sizeof(int))));
               cout << "(" << currentPage << "," << currentSlot << ")";
               offset += entrySize + 2*sizeof(int);
            }else{
               //This is a new key we are considering.
               cout << "]\",\"";
               memset(currentKeyBuffer, 0, entrySize);
               memcpy(currentKeyBuffer, &nextKey, sizeof(int));
               cout << nextKey << ": [";
            }

         }
         cout << "]\"]}" <<endl;
         free(currentKeyBuffer);
      }
   } else {
   //The page we are looking at is a node.
      cout << "This is a node" << endl;
      if(attribute.type == TypeVarChar){
         cout << "An varChar node more specifically" << endl;
         //We are dealing with a type VarChar node.
         vector<int> children;
         vector<int> temp;
         cout << "{\"keys\":[";
         int offset = 0;
         int entrySize;
         int varLength;
         //We are dealing with type Varchar node.
         while (offset < occupiedSpace - sizeof(int)){
            if(offset > sizeof(int)){
               cout << ",";
            }
            int pagePointer = (*(int *)((char *) pageData + offset));
            varLength = (*(int *)((char *) pageData + offset + sizeof(int)));
            entrySize = varLength + sizeof(int);
            temp.push_back(pagePointer);
            offset += 2*sizeof(int);
            cout << "before loop" << endl;
            for (int i = offset; i < *(int*)((char*) pageData + (offset + varLength)); i++){
               cout << *((char *) pageData + offset + i);
            }
            cout << "after loop" << endl;
            //Need to adjust offset after printing.
            offset -= sizeof(int);
            //adjust offset to accomidate the size of the varChar string.
            offset += entrySize;
            //Moving past RID
            offset += 2*sizeof(int);
         }
         //Get the last page pointer from the node.
         temp.push_back((*(int *)((char *) pageData + occupiedSpace - sizeof(int))));
         cout << "]," << endl;
         cout << "\"children\": [" << endl << "\t";
         //Putting the children in the right order.
         while(!temp.empty()){
            cout << "AM I stuck here" << endl;

            children.push_back(temp.back());
            temp.pop_back();
         }
         while(!children.empty()){
            cout << "AM I stuck here" << endl;

            int nextPage = children.back();
            void* nextPageBuffer  = calloc(PAGE_SIZE,1);
            ixfileHandle.ixReadPage(nextPage, nextPageBuffer);
            helpPrint(ixfileHandle, attribute, nextPageBuffer);
            free(nextPageBuffer);
            cout << "AM I stuck here" << endl;
         }
         cout << "]}" << endl;
      }else{
         //We are dealing with type int or float node.
         cout << "An int node more specifically" << endl;
         vector<int> temp;
         vector<int> children;
         cout << "{\"keys\":[";
         int offset = 0;
         while (offset < occupiedSpace - sizeof(int)){
            if(offset > sizeof(int)){
               cout << ",";
            }
            int pagePointer = (*(int *)((char *) pageData + offset));
            temp.push_back(pagePointer);
            int key = (*(int *)((char *) pageData + offset + sizeof(int)));
            cout << "\""<< key << "\"" << endl;
            offset += key + 3*sizeof(int);
         }
         //Get the last page pointer in this node
         temp.push_back((*(int *)((char *) pageData + occupiedSpace - sizeof(int))));
         cout << "]," << endl;
         cout << "\"children\": [" << endl << "\t";
         //Putting the children in the right order.
         while(!temp.empty()){
            children.push_back(temp.back());
         }
         while(!children.empty()){
            int nextPage = children.back();
            void* nextPageBuffer  = calloc(PAGE_SIZE,1);
            ixfileHandle.ixReadPage(nextPage, nextPageBuffer);
            helpPrint(ixfileHandle, attribute, nextPageBuffer);
            free(nextPageBuffer);
         }
         cout << "]}" << endl;
      }
   }
return;
>>>>>>> 572220b649415de35e5481876b80701f76afd54d
}

void IndexManager::tempPrintBtree(IXFileHandle &ixfileHandle, const Attribute &attribute){
   int cur = 0, total = ixfileHandle.getNumberOfPages();
   void * pageData = malloc(PAGE_SIZE);


   cout << "-----------------------------------------" << endl;
   for(cur = 0; cur < total; cur++){
      ixfileHandle.ixReadPage(cur, pageData);
      cout << "Page: " << cur << endl;
      cout << "LeftPagePointer: " << *(int *) pageData << endl;
      cout << "RightPagePointer: " << *(int *) ((char *) pageData + PAGE_SIZE - 2*sizeof(int)) << endl;
      cout << "TotalSize: " << getTotalSize(pageData) << endl;
      int offset = 4;


      while(offset < getTotalSize(pageData) - PAGE_DEFAULT + sizeof(int)){
         cout << "Offset: " << offset << endl;
         switch(attribute.type){
            case TypeInt:
               cout << "Int Data: " << *(int *)((char *) pageData + offset) << endl;
               offset += sizeof(int);
               cout << "RID Page: " << *(int *)((char *) pageData + offset) << endl;
               offset += sizeof(int);
               cout << "RID Slot: " << *(uint32_t *)((char *) pageData + offset) << endl;
               offset += sizeof(int);
               if(isLeaf(pageData) < 0){
                  cout << "RightPagePointer: " << *(int *)((char *) pageData + offset) << endl;
                  offset += sizeof(int);
               }
               cout << endl;
               break;
            case TypeReal:
               cout << "Float Data: " << *(float *)((char *) pageData + offset) << endl;
               offset += sizeof(int);
               cout << "RID Page: " << *(int *)((char *) pageData + offset) << endl;
               offset += sizeof(int);
               cout << "RID Slot: " << *(uint32_t *)((char *) pageData + offset) << endl;
               offset += sizeof(int);
               if(isLeaf(pageData) < 0){
                  cout << "RightPagePointer: " << *(int *)((char *) pageData + offset) << endl;
                  offset += sizeof(int);
               }
               cout << endl;
               break;
            case TypeVarChar:
               size_t len = *(int *)((char *)pageData + offset);
               cout << "Length: " << len << endl;
               cout << "String Data: ";
               for(int i = 0; i < len; i++){
                  cout << *((char *) pageData + offset + i + 4);
               }
               cout << endl;

               offset += len + sizeof(int);
               cout << "RID Page: " << *(int *)((char *) pageData + offset) << endl;

               offset += sizeof(int);
               cout << "RID Slot: " << *(uint32_t *)((char *) pageData + offset) << endl;

               offset += sizeof(int);
               if(isLeaf(pageData) < 0){
                  cout << "RightPagePointer: " << *(int *)((char *) pageData + offset) << endl;
                  offset += sizeof(int);
               }
               cout << endl;
               break;
         }
      }
      if(isLeaf(pageData) == -1){

      }
      cout << "-----------------------------------------" << endl;

   }

   free(pageData);
}


























/***********************************************************************
                           IX Helper Functions
***********************************************************************/

/*
* Function: parseIXReturn
* -----------------------------
* If the DEBUG flag is set, prints out the associated IX_Returns message
*
* Arguments: ret
* ret: The return value of an IX function to parse
*
* Return: RC
* Returns the same return value as was passed in
*/
RC IndexManager::parseIXReturn(string funcName, IX_Returns ret){
   if(DEBUG){
      switch (ret){
         case IX_NOT_IMPLEMENTED:
            cout << "ERROR: " << funcName << " not implemented" << endl;
            break;
         case IX_SUCCESS:
            cout << funcName << " ran successfully" << endl;
            break;
         case IX_CREATE_FAILED:
            cout << "ERROR: " << funcName <<
               " could not create file " << errArg << endl;
            break;
         case IX_MALLOC_FAILED:
            cout << "ERROR: " << funcName <<
               " could not malloc required space" << endl;
            break;
         case IX_OPEN_FAILED:
            cout << "ERROR: " << funcName << " could not open file "
               << errArg << endl;
            break;
         case IX_APPEND_FAILED:
            cout << "ERROR: " << funcName <<
               " could not append to file " << errArg << endl;
            break;
         case IX_READ_FAILED:
            cout << "ERROR: " << funcName
               << " could not read in page " << errArg << endl;
            break;
         case IX_DELETE_FAILED:
            cout << "ERROR: " << funcName <<
               " could not delete file " << errArg << endl;
            break;
         case IX_NO_DUP_KEY:
            cout << "ERROR: " << funcName <<
            " could not insert duplicate key " << errArg << endl;
            break;
         case IX_HANDLE_IN_USE:
            cout << "ERROR: " << funcName << " ixHandle in use for "
               << errArg << endl;
            break;
         case IX_FILE_DNE:
            cout << "ERROR: " << funcName << " file " << errArg <<
               " does not exist" << endl;
            break;
         case IX_PAGE_NOT_FOUND:
            cout << "ERROR: " << funcName << " no page matches key "
               << endl;
            break;
         case IX_KEY_NOT_FOUND:
            cout <<"ERROR: " << funcName << " no matches for key "
               << endl;
            break;
         case IX_NULL_HANDLE:
            cout << "ERROR: " << funcName << " NULL file handle" << endl;
            break;
      }
   }

   return(ret);
}

/*
* Function: createNewPage
* -----------------------------
* Sets the information for a blank page pointed to by page, sets this
* page as a leaf if the boolean is set
*
* Arguments: page, leaf
* page: pointer to the page to setup
* leaf: indicates whether the new page is to be a leaf or node
* left: The page number of the leaf to the left in the linked list
* right: The page number of the leaf to the right in the linked list
*
* Return: none
*/
void IndexManager::createNewPage(void * page, bool leaf, int left, int right){
  //Passes our newly allocated memory of 4096 bytes pointer to the setTotalSize function. With integer of 8.
   setTotalSize(page, PAGE_DEFAULT);

   if(leaf){
      setAsLeaf(page, left, right);
   }else{
      setAsNode(page);
   }
}

/*
* Function: setTotalSize
* -----------------------------
* Sets the size slot on the given page to be the value in size
*
* Arguments: page, size
* page: pointer to the page to set
* size: value to set in the size field
*
* Return: none
*/
void IndexManager::setTotalSize(void * page, int size){
  //page + PAGE_SIZE takes our pointer to 4096 bytes of memory and moves us 4096 bytes into the page.
  //The -sizeof(int) then takes us back 4 bytes where we will begin recording the page size for every page.
  //The &size alocates 8 bytes at the end of each page so that we have room for the size and isLeaf.
   memcpy(((char *) page + PAGE_SIZE - sizeof(int)), &size, sizeof(int));
}

/*
* Function: getTotalSize
* -----------------------------
* Returns the size field off of the given page
*
* Arguments: page
* page: pointer to the page to be read
*
* Return: integer
* The int is the integer representation of the size field on page
*/
int IndexManager::getTotalSize(void * page){
  //Reads the last size of int bytes of the page in an integer format to get the total page size.
   return(*(int *)((char *) page + PAGE_SIZE - sizeof(int)));
}

/*
* Function: fileExists
* -----------------------------
* Checks to see if a file with name fileName exists in the current dir
*
* Arguments: fileName
* fileName: The string to search for
*
* Return: boolean
* The bool represents if the file was found (1) or not (0)
*/
bool IndexManager::fileExists(const string &fileName)
{
    // If stat fails, we can safely assume the file doesn't exist
    struct stat sb;
    return stat(fileName.c_str(), &sb) == 0;
}

/*
* Function: setAsLeaf
* -----------------------------
* Sets the page pointer values to be >= 0 to show as leaf, pointers that
* point to themselves are ends of the list
*
* Arguments: page, left, right
* page: The page to set as a leaf
* left: The page number of the leaf to the left in the linked list
* right: The page number of the leaf to the right in the linked list
*
* Return:
*/
void IndexManager::setAsLeaf(void * page, int left, int right){
   memcpy(page, &left, sizeof(int));
   memcpy((char *) page + PAGE_SIZE - 2*sizeof(int), &right, sizeof(int));
}

/*
* Function: setAsNode
* -----------------------------
* Sets the value on the page to -1 to desginate this page as a node
*
* Arguments: page
* page: The page to set as a node
*
* Return:
*/
void IndexManager::setAsNode(void * page){
  //Similar to the SetAsLeaf function but sets the bytes to -1 intead.
  memset(page, -1, sizeof(int));
  memset((char *) page + PAGE_SIZE - 2*sizeof(int), -1, sizeof(int));
}

/*
* Function: isLeaf
* -----------------------------
* Checks to see if the given page is a leaf
*
* Arguments: page
* fileName: The page to check
*
* Return: Integer
* The int represents if the file is a leaf (>= 0) or a node (-1)
*/
int IndexManager::isLeaf(void * page){
  //Point to the 4-8 bytes at the end of the page and read it as an integer value.
   return(*(int *)((char *) page + PAGE_SIZE - 2*sizeof(int)));
}

/*
* Function: traverseTree
* -----------------------------
* Returns the leaf page on which the key is or should be a key,RID pair
*
* Arguments: val, attr, handle, pageTrace
* val: key value to traverse by
* attr: Attribute to define what the key is
* handle: the file handle of the file containing the tree to traverse
* pageTrace: a vector with the order of pages searched to the leaf
*
* Return: integer
* The page number of the leaf to insert on, returns negative if not found
*/
int IndexManager::traverseTree(const void *val, Attribute attr,
      IXFileHandle handle, vector<int> &pageTrace){

   size_t trafficCopLength;
   int curPage = 0;
   void * pageData = NULL;
   int offset;

   //Clean up the vector to make sure there is no residual path
   pageTrace.clear();

   //Create a page sized buffer to read data into
   pageData = calloc(PAGE_SIZE, 1);
   if(!pageData){
      return (parseIXReturn("IndexManager::traverseTree", IX_MALLOC_FAILED));
   }

   //Loop until page is found
   while(1){
      //Read in the specified page starting with the root (0)
      if (handle.ixReadPage(curPage, pageData)){
         errArg = to_string(curPage);
         free(pageData);
         return (parseIXReturn("IndexManager::traverseTree", IX_READ_FAILED));
      }

      //Push the curPage on the back of the pages searched
      pageTrace.push_back(curPage);

      //If page is a node, check traffic cops until traffic cop key val
      //is less than search key or pointer is past end of data --> search
      //key value > all traffic cops --> take last pointer
      if(isLeaf(pageData) < 0){
         offset = sizeof(int);
         curPage = -1;
         //while still in valid data, totalSize includes the two integer
         //fields at the end and sizeof(RID) prevents us from reading the
         //final pointer as a key
         while(offset < getTotalSize(pageData) - PAGE_DEFAULT){
            //If the search value is less than the traffic cop value
            if(compareKeys(val, (void *)((char *)pageData + offset), attr, 0) < 0){
               curPage = *(int *)((char *) pageData + offset - sizeof(int));
               break;
            }

            //Setup and increment the offset
            //Increment by key value (or size) and the RID
            trafficCopLength = sizeof(int) + sizeof(RID) + sizeof(int);
            //If it is a varchar, read in and add length
            if(attr.type == TypeVarChar){
               trafficCopLength += *(int *)((char *)pageData + offset);
            }

            offset += trafficCopLength;
         }

         //If curPage is -1 after going through all entries, then the
         //key is larger than every one in node so take last pointer
         if(curPage == -1){
            curPage = *(int *)((char *) pageData + offset - sizeof(int));
         }
      }


      //Otherwise the page is the correct leaf and should be returned
      else{
         free(pageData);
         return(curPage);
      }

      //If the same page has been pushed, that means the same page  has
      //been hit multiple times, something that should not happen
      if(pageTrace.size() > 1 &&
         pageTrace.at(pageTrace.size() - 1) == pageTrace.at(pageTrace.size() - 2)){

         break;
      }
   }

   free(pageData);
   return (parseIXReturn("IndexManager::traverseTree", IX_PAGE_NOT_FOUND));
}

/*
* Function: compareKeys
* -----------------------------
* Compares the two keys given by the pointers
*
* Arguments: first, second, attr
* first: the first argument to compare
* second: the second argument to compare
* attr: the attributes of both keys
*
* Return: Integer
* returns -1 if first < second, 0 if first = second and 1 if first > second
*/
int IndexManager::compareKeys(const void * first, const void* second, Attribute attr, bool isPair){
   int firstInt = 0, secondInt = 0;
   float firstFloat = 0, secondFloat = 0;

   switch(attr.type){
      case TypeInt:
         firstInt = *(int *)first;
         secondInt = *(int *)second;
         if(isPair && firstInt == secondInt){
            return(memcmp(((int *) first + 1), ((int *) second + 1), sizeof(int)));
         }
         return((firstInt < secondInt) ? -1 : (firstInt == secondInt) ? 0 : 1);
         break;
      case TypeReal:
         firstFloat = *(float *)first;
         secondFloat = *(float *)second;
         if(isPair && firstFloat == secondFloat){
            return(memcmp(((float *) first + 1), ((float *) second + 1), sizeof(int)));
         }
         return((firstFloat < secondFloat) ? -1 : (firstFloat == secondFloat) ? 0 : 1);
         break;
      case TypeVarChar:
         size_t firstStrLen = *(int *)first;
         size_t secondStrLen = *(int *)second;
         RID * firstRid;
         RID * secondRid;
         int retVal = memcmp(((char *)first + sizeof(int)),
            ((char *)second + sizeof(int)),
            min(firstStrLen, secondStrLen));
         if(isPair && retVal == 0){
            firstRid = (RID *)((char *)first + sizeof(int) + firstStrLen);
            secondRid = (RID *)((char *)second + sizeof(int) + secondStrLen);
            if(firstRid->pageNum == secondRid->pageNum){
               return(firstRid->slotNum - secondRid->slotNum);
            }else{
               return(firstRid->pageNum - secondRid->pageNum);
            }
         }
         return(retVal);
         break;
   }
}




/*
* Function: insertPair
* -----------------------------
* Assumes there is space on the page and attempts to insert the pair
*
* Arguments: pageData, searchPair, attribute, entrySize, ixfileHandle, leafPage
* pageData: a pointer to a PAGE_SIZE allocated buffer with data from leafPage
* searchPair: The pair to compare against and insert
* attribute: A description of the searchPair key
* entrySize: The length in bytes of the searchPair DOES NOT INCLUDE RID
* ixfileHandle: the file handle of the tree file
* leafPage: the integer representing the current page
*
* Return: Integer
* returns a negative value for errors, 0 on success
*/
RC IndexManager::insertPair(void * searchPair, const Attribute &attribute,
   int entrySize, IXFileHandle &ixfileHandle, int leafPage){


   int offset = sizeof(int);
   int occupiedSpace;
   void * pageData;

   cout << "IN insertPair";

   if(attribute.type == TypeVarChar){
      entrySize = sizeof(int) + *(int *)searchPair + sizeof(RID);
   }

   //Create a page sized buffer to read data into
   pageData = calloc(PAGE_SIZE, 1);
   if(!pageData){
      return (parseIXReturn("IndexManager::traverseTree", IX_MALLOC_FAILED));
   }

   //Read the page
   if (ixfileHandle.ixReadPage(leafPage, pageData)){
      errArg = to_string(leafPage);
      free(pageData);
      return (parseIXReturn("IndexManager::traverseTree", IX_READ_FAILED));
   }

   occupiedSpace = getTotalSize(pageData);

   while(offset < getTotalSize(pageData) - 2*sizeof(int)){

      //Error to have matching keys
      if(compareKeys(searchPair, ((char *) pageData + offset), attribute, 1) == 0){
         free(pageData);
         return (parseIXReturn("IndexManager::insertPair", IX_NO_DUP_KEY));
      }
      //If the page key is smaller, than place key before this one
      if(compareKeys(searchPair, ((char *) pageData + offset), attribute, 1) < 0){
         //Move the records down the page to make room for our new entry.
         memmove(((char *)pageData + (offset + entrySize +sizeof(RID))),
            ((char *)pageData + offset),
            getTotalSize(pageData) - PAGE_DEFAULT - offset + sizeof(int)); //Adding back the sizeof(int) to account for page pointer at beginning.

         //insert the entry into our page.
         memcpy(((char *)pageData + offset), searchPair, entrySize + sizeof(RID));
         setTotalSize(pageData, occupiedSpace + entrySize + sizeof(RID));
         ixfileHandle.ixWritePage(leafPage, pageData);

         free(pageData);
         return(parseIXReturn("IndexManager::insertPair", IX_SUCCESS));
      }

      if(attribute.type == TypeVarChar){
         offset += *(int *)((char *)pageData + offset);
      }
      offset += sizeof(int) + sizeof(RID);


   }
    //If we get here, this means that the key is greater than every key
    //so place at end
    memcpy(((char *) pageData + sizeof(int) + getTotalSize(pageData) - PAGE_DEFAULT), searchPair, entrySize + sizeof(RID));
    setTotalSize(pageData, occupiedSpace + entrySize + sizeof(RID));
    ixfileHandle.ixWritePage(leafPage, pageData);

    free(pageData);
    return(parseIXReturn("IndexManager::insertPair", IX_SUCCESS));
}


RC IndexManager::insertPairVar(void * searchPair, const Attribute &attribute, int entrySize,
  IXFileHandle &ixfileHandle, int leafPage){

  int offset = sizeof(int);
  int occupiedSpace;
  void * pageData;
  int PrevEntrySize;
  int varNextEntryLength;

  //Create a page sized buffer to read data into
  pageData = calloc(PAGE_SIZE, 1);
  if(!pageData){
     return (parseIXReturn("IndexManager::traverseTree", IX_MALLOC_FAILED));
  }

  if (ixfileHandle.ixReadPage(leafPage, pageData)){
     errArg = leafPage;
     free(pageData);
     return (parseIXReturn("IndexManager::traverseTree", IX_READ_FAILED));
  }

  occupiedSpace = getTotalSize(pageData);

  while(offset < getTotalSize(pageData) - 2*sizeof(int)){
    //Error to have matching keys
   cout << "I am here!!!!!" << endl;
    if(compareKeys(searchPair, ((char *) pageData + offset), attribute, 1) == 0){
      free(pageData);
      return (parseIXReturn("IndexManager::insertPair", IX_NO_DUP_KEY));
    }
    //If the page key is smaller, than place key before this one
    cout << "Before CompareKeys!!!!!!" << endl;
    if(compareKeys(searchPair, ((char *) pageData + offset), attribute, 1) < 0){
      cout << "After CompareKeys!!!!!!" << endl;
      //Move the records down the page to make room for our new entry.
      memmove(((char *)pageData + (offset + entrySize +sizeof(RID))),
      ((char *)pageData + offset),
      getTotalSize(pageData) - PAGE_DEFAULT - offset + sizeof(int)); //Adding back the sizeof(int) to account for page pointer at beginning.

      //insert the entry into our page.
      memcpy(((char *)pageData + offset), searchPair, entrySize + sizeof(RID));
      setTotalSize(pageData, occupiedSpace + entrySize + sizeof(RID));
      ixfileHandle.ixWritePage(leafPage, pageData);

      free(pageData);
      return(parseIXReturn("IndexManager::insertPair", IX_SUCCESS));
    }
    cout << "Black Guys!!!" << endl;
    varNextEntryLength = (*(int *)((char *) pageData + offset));
    PrevEntrySize = sizeof(int) + varNextEntryLength;
    offset += PrevEntrySize + sizeof(RID);
  }
  //If we get here, this means that the key is greater than every key
  //so place at end
   cout << "We got HERE" << endl;
   cout << getTotalSize(pageData)<< endl;
   memcpy(((char *) pageData + (sizeof(int) + getTotalSize(pageData) - PAGE_DEFAULT)), searchPair, entrySize + sizeof(RID));
   cout << "After memcpy" << endl;
   setTotalSize(pageData, occupiedSpace + entrySize + sizeof(RID));
   cout << "After setter" << endl;
   ixfileHandle.ixWritePage(leafPage, pageData);
   cout << "After Write" << endl;

   free(pageData);
   return(parseIXReturn("IndexManager::insertPair", IX_SUCCESS));
}


/*
* Function: splitLeaf
* -----------------------------
* Splits the given leaf into two separate leaves and links them
*
* Arguments: handle, curPageNum, attr, pair
* handle: the file handle to the tree file
* curPageNum: the current page in the file to split
* attr: the attributes of the key in the pair
* pair: the key RID buffer
*
* Return: Integer
* returns the page number of the created page
*/
int IndexManager::splitLeaf(IXFileHandle handle, int curPageNum,
   Attribute attr, void *pair){

   void *pageData = NULL;
   void *newPage = NULL;
   void *nextPage = NULL;
   int nextPageNum;
   int newPageNum;
   int offset;
   int pairSize;

   //Malloc space for current page buffer
   pageData = calloc(PAGE_SIZE, 1);
   if (pageData == NULL){
      return (parseIXReturn("IndexManager::splitLeaf", IX_MALLOC_FAILED));
   }

   //Read in the current page
   if(handle.ixReadPage(curPageNum, pageData)){
      free(pageData);
      errArg = to_string(curPageNum);
      return (parseIXReturn("IndexManager::splitLeaf", IX_READ_FAILED));
   }

   //Malloc space for new page
   newPage = calloc(PAGE_SIZE, 1);
   if (newPage == NULL){
      free(pageData);
      return (parseIXReturn("IndexManager::splitLeaf", IX_MALLOC_FAILED));
   }

   //Create an empty leaf page with temporary page pointers
   createNewPage(newPage, 1, curPageNum, curPageNum);
   //And append it to the end so it has pageNum totalPages - 1
   handle.ixAppendPage(newPage);
   newPageNum = handle.getNumberOfPages() - 1;

   //See if there is a list continuing to the right
   nextPageNum = isLeaf(pageData);

   //If there is a list
   if(nextPageNum != curPageNum){
      //Malloc a buffer for the page to the right of the current one
      nextPage = calloc(PAGE_SIZE, 1);
      if (nextPage == NULL){
         free(pageData);
         free(newPage);
         return (parseIXReturn("IndexManager::splitLeaf", IX_MALLOC_FAILED));
      }

      //Read in the right page
      if(handle.ixReadPage(nextPageNum, nextPage)){
         free(pageData);
         free(newPage);
         free(nextPage);
         errArg = to_string(nextPageNum);
         return (parseIXReturn("IndexManager::splitLeaf", IX_READ_FAILED));
      }

      //Set the current right page pointer to the new page
      //And leave left pointer alone
      setAsLeaf(pageData, *(int *)pageData, newPageNum);

      //Set the newPage's left pointer to the current page and the
      //right pointer to the former right page of the current one
      setAsLeaf(newPage, curPageNum, nextPageNum);

      //Set the nextPage left pointer to be the new page and leave
      //the right page pointer alone
      setAsLeaf(nextPage, newPageNum, isLeaf(nextPage));

      //Don't need the nextPage anymore so write and free it
      handle.ixWritePage(nextPageNum, nextPage);
      free(nextPage);
   }
   //Check to see if this is the root, special case here
   else if(curPageNum == 0){
      void * secondNewPage = NULL;
      int secondNewPageNum;

      //Malloc space for second new page
      secondNewPage = calloc(PAGE_SIZE, 1);
      if (secondNewPage == NULL){
         free(pageData);
         free(newPage);
         return (parseIXReturn("IndexManager::splitLeaf", IX_MALLOC_FAILED));
      }

      //Create an empty leaf page with temporary page pointers
      createNewPage(secondNewPage, 1, curPageNum, curPageNum);
      //And append it to the end so it has pageNum totalPages - 1
      handle.ixAppendPage(secondNewPage);
      secondNewPageNum = handle.getNumberOfPages() - 1;

      //Set the root as a node
      setAsNode(pageData);
      //Copy all of the root data to the left leaf
      memcpy(newPage, pageData, PAGE_SIZE);
      //Clear the root
      memset(pageData, 0, PAGE_SIZE);
      //And rewrite as node
      createNewPage(pageData, 0, 0, 0);
      //With left pointer set to the left leaf
      *(int *)pageData = newPageNum;

      //Set the left pointer to itself since leftmost in list, set right
      //to be the second new page
      setAsLeaf(newPage, newPageNum, secondNewPageNum);

      //Set the left pointer to the firs new page and the right to itself
      setAsLeaf(secondNewPage, newPageNum, secondNewPageNum);

      //Write the changes to root and left page
      handle.ixWritePage(curPageNum, pageData);
      handle.ixWritePage(newPageNum, newPage);


      //Don't need the root pointer anymore
      free(pageData);

      //But set it to be the left node so code works the same
      pageData = newPage;
      curPageNum = newPageNum;
      //And set newPage to point at the secondNewPage similarly
      newPage = secondNewPage;
      newPageNum = secondNewPageNum;

      //secondNewPage values irrelevant now
      secondNewPage = NULL;
      secondNewPageNum = -1;
   }
   //Otherwise there is no list to the right of the current leaf page
   else{
      //Set the current right page pointer to the new page
      //And leave left pointer alone
      setAsLeaf(pageData, *(int *)pageData, newPageNum);

      //Set the newPage's left pointer to the current page and the
      //right pointer to itself to signify end of linked list
      setAsLeaf(newPage, curPageNum, newPageNum);
   }

   //Now need to split the entries evenly between the two pages
   //Skipping page pointer
   offset = sizeof(int);

   //While offset is less than the half of the way through the page
   while(offset < getTotalSize(pageData)/2){
      //Increment the offset by one pair at a time
      if(attr.type == TypeVarChar){
         offset += *(int *)((char *) pageData + offset);
      }
      offset += sizeof(int) + sizeof(RID);
   }

   //Once halfway through
   //Copy the latter half of the data to the next page, exluding end values
   memcpy(((char *)newPage + sizeof(int)), ((char *)pageData + offset),
      getTotalSize(pageData) - offset - PAGE_DEFAULT + sizeof(int));

   //Find size of the pair
   pairSize = sizeof(int);
   if(attr.type == TypeVarChar){
      pairSize += *(int *)((char *) pageData + offset);
   }

   //Set the new sizes
   setTotalSize(newPage, getTotalSize(pageData) - offset + sizeof(int));
   setTotalSize(pageData, offset + 2*sizeof(int));

   //Clean out the copied data space
   memset(((char *)pageData + offset), 0, PAGE_SIZE - offset - 2*sizeof(int));

   //Write the new data
   handle.ixWritePage(curPageNum, pageData);
   handle.ixWritePage(newPageNum, newPage);


   //Insert on the correct page
   if(compareKeys(pair, ((char *)pageData + offset), attr, 1) < 0){
      insertPair(pair, attr, pairSize, handle, curPageNum);
   } else{
      insertPair(pair, attr, pairSize, handle, newPageNum);
   }

   free(pageData);
   free(newPage);

   return(newPageNum);
}

/*
* Function: insertIntoNode
* -----------------------------
* Assumes there is space on the node page and enters it
*
* Arguments: handle, pair, attr, nodePage, pagePtr
* handle: the file handle for the file containing the tree
* pair: the key RID pair
* attr: the attribute describing the key
* nodePage: the pageNum of the node to insert into
* pagePtr: the pageNum to set as the left pagePtr
*
* Return: Integer
* returns a negative value for errors, 0 on success
*/
int IndexManager::insertIntoNode(IXFileHandle handle, void * pair,
   Attribute attr, int nodePage, int pagePtr){

   void * pageData = NULL;
   void * entry = NULL;
   size_t entrySize;
   int offset;

   //Create a page sized buffer to read data into
   pageData = calloc(PAGE_SIZE, 1);
   if(!pageData){
      return (parseIXReturn("IndexManager::insertIntoNode", IX_MALLOC_FAILED));
   }

   //Read the nodePage into the buffer
   if (handle.ixReadPage(nodePage, pageData)){
      errArg = to_string(nodePage);
      free(pageData);
      return (parseIXReturn("IndexManager::insertIntoNode", IX_READ_FAILED));
   }

   //Calculate the length of the entire entry to be added to the node
   entrySize = 2*sizeof(int) + sizeof(RID);
   if(attr.type == TypeVarChar){
      entrySize += *(int *)pair;
   }

   //Malloc a buffer to put the entry into
   entry = calloc(entrySize, 1);
   if(!entry){
      return (parseIXReturn("IndexManager::insertIntoNode", IX_MALLOC_FAILED));
   }
   //Fill the buffer
   memcpy(entry, pair, entrySize - sizeof(int));
   memcpy(((char *)entry + entrySize - sizeof(int)), &pagePtr, sizeof(int));

   //Find where the entry goes on the page
   offset = sizeof(int);
   while(offset < getTotalSize(pageData) - 2*sizeof(int)){
     //Error to have matching keys
     if(compareKeys(pair, ((char *) pageData + offset), attr, 1) == 0){
       free(pageData);
       return (parseIXReturn("IndexManager::insertIntoNode", IX_NO_DUP_KEY));
     }
     //If the page key is smaller, than place key before this one
     if(compareKeys(pair, ((char *) pageData + offset), attr, 1) < 0){
       //Move the records down the page to make room for our new entry.
       memmove(((char *)pageData + (offset + entrySize)),
       ((char *)pageData + offset),
       getTotalSize(pageData) - PAGE_DEFAULT - offset + sizeof(int));

       //insert the entry into our page.
       memcpy(((char *)pageData + offset), entry, entrySize);
       setTotalSize(pageData, getTotalSize(pageData) + entrySize);
       handle.ixWritePage(nodePage, pageData);

       free(pageData);
       return(parseIXReturn("IndexManager::insertIntoNode", IX_SUCCESS));
     }

     //Increment the offset
     if(attr.type == TypeVarChar){
        offset += *(int *)((char *) pageData + offset);
     }
     offset += 2*sizeof(int) + sizeof(RID);
   }

   //If we get here, this means that the key is greater than every key
   //so place at end
   memcpy(((char *) pageData + sizeof(int) + getTotalSize(pageData) - PAGE_DEFAULT), entry, entrySize);
   setTotalSize(pageData, getTotalSize(pageData) + entrySize);
   handle.ixWritePage(nodePage, pageData);

   free(pageData);
   return(parseIXReturn("IndexManager::insertIntoNode", IX_SUCCESS));
}






int IndexManager::splitNode(IXFileHandle handle, int curPageNum,
   Attribute attr, void *pair, int pagePtr, int parentPtr){

   void *pageData = NULL;
   void *newPage = NULL;
   int newPageNum;
   int offset;
   int pairSize;

   //Malloc space for current page buffer
   pageData = calloc(PAGE_SIZE, 1);
   if (pageData == NULL){
      return (parseIXReturn("IndexManager::splitNode", IX_MALLOC_FAILED));
   }

   //Read in the current page
   if(handle.ixReadPage(curPageNum, pageData)){
      free(pageData);
      errArg = to_string(curPageNum);
      return (parseIXReturn("IndexManager::splitNode", IX_READ_FAILED));
   }

   //Malloc space for new page
   newPage = calloc(PAGE_SIZE, 1);
   if (newPage == NULL){
      free(pageData);
      return (parseIXReturn("IndexManager::splitNode", IX_MALLOC_FAILED));
   }

   //Create an empty node page
   createNewPage(newPage, 0, 0, 0);
   //And append it to the end so it has pageNum totalPages - 1
   handle.ixAppendPage(newPage);
   newPageNum = handle.getNumberOfPages() - 1;

   //Check to see if this is the root, special case here
   if(curPageNum == 0){
      void * secondNewPage = NULL;
      int secondNewPageNum;

      parentPtr = 0;

      //Malloc space for second new page
      secondNewPage = calloc(PAGE_SIZE, 1);
      if (secondNewPage == NULL){
         free(pageData);
         free(newPage);
         return (parseIXReturn("IndexManager::splitNode", IX_MALLOC_FAILED));
      }

      //Create an empty node page
      createNewPage(secondNewPage, 0, 0, 0);
      //And append it to the end so it has pageNum totalPages - 1
      handle.ixAppendPage(secondNewPage);
      secondNewPageNum = handle.getNumberOfPages() - 1;

      //Copy all of the root data to the left node
      memcpy(newPage, pageData, PAGE_SIZE);
      //Clear the root
      memset(pageData, 0, PAGE_SIZE);
      //And rewrite
      createNewPage(pageData, 0, 0, 0);
      //With left pointer set to the left node
      *(int *)pageData = newPageNum;

      //Write the changes to root and left page
      handle.ixWritePage(curPageNum, pageData);
      handle.ixWritePage(newPageNum, newPage);

      //Don't need the root pointer anymore
      free(pageData);

      //But set it to be the left node so code works the same
      pageData = newPage;
      curPageNum = newPageNum;
      //And set newPage to point at the secondNewPage similarly
      newPage = secondNewPage;
      newPageNum = secondNewPageNum;

      //secondNewPage values irrelevant now
      secondNewPage = NULL;
      secondNewPageNum = -1;
   }

   //Now need to split the entries evenly between the two pages
   //Skipping page pointer
   offset = sizeof(int);
   //While offset is less than the half of the way through the page
   while(offset < getTotalSize(pageData)/2){
      //Increment the offset by one pair at a time
      if(attr.type == TypeVarChar){
         offset += *(int *)((char *) pageData + offset);
      }
      offset += 2*sizeof(int) + sizeof(RID);
   }

   //Once halfway through
   //Copy the latter half of the data to the next page, exluding end values
   memcpy(((char *)newPage + sizeof(int)), ((char *)pageData + offset),
      getTotalSize(pageData) - offset - PAGE_DEFAULT + sizeof(int));

   //Set the new sizes
   setTotalSize(newPage, getTotalSize(pageData) - offset + sizeof(int));
   setTotalSize(pageData, offset + 2*sizeof(int));

   //Clean out the copied data space
   memset(((char *)pageData + offset), 0, PAGE_SIZE - offset - 2*sizeof(int));

   //Write the new data
   handle.ixWritePage(curPageNum, pageData);
   handle.ixWritePage(newPageNum, newPage);


   //Insert on the correct page
   if(compareKeys(pair, ((char *)pageData + offset), attr, 1) < 0){
      insertIntoNode(handle, pair, attr, curPageNum, pagePtr);
   } else{
      insertIntoNode(handle, pair, attr, newPageNum, pagePtr);
   }

   //Now check parent node for space
   //Find size of the pair
   pairSize = 2*sizeof(int) + sizeof(RID);
   if(attr.type == TypeVarChar){
      pairSize += *(int *)((char *) pageData + offset);
   }

   //Read the parent page into the buffer
   if (handle.ixReadPage(parentPtr, pageData)){
      errArg = to_string(parentPtr);
      free(pageData);
      free(newPage);
      return (parseIXReturn("IndexManager::insertIntoNode", IX_READ_FAILED));
   }

   //If there is not enough space, recursively call until there is
   if(pairSize > PAGE_SIZE - getTotalSize(pageData)){
      splitNode(handle, parentPtr, attr, pair, newPageNum, 0);
   }
   //Otherwise, insert into the parent
   else{
      insertIntoNode(handle, ((char *)newPage + sizeof(int)), attr, parentPtr, newPageNum);
   }


   free(pageData);
   free(newPage);

   return(newPageNum);

}

















/***********************************************************************
                           IX_ScanIterator
***********************************************************************/
IX_ScanIterator::IX_ScanIterator()
{
}

IX_ScanIterator::~IX_ScanIterator()
{
}


//This method should set its output parameters rid and key to be the RID
// and key, respectively, of the next record in the index scan. This
//method should return IX_EOF if there are no index entries left
//satisfying the scan condition. You may assume that IX component
//clients will not close the corresponding open index while a scan is
//underway. All keys should be printed in ascending order.
RC IX_ScanIterator::getNextEntry(RID &rid, void *key)
{
   //At end of the current page
   if(curOffset >= _ix->getTotalSize(pageData) - PAGE_DEFAULT + sizeof(int)){
      if(_ix->isLeaf(pageData) != curPage){
         curPage = _ix->isLeaf(pageData);
         curOffset = sizeof(int);

         //Read in the new page
         if (iHandle.ixReadPage(curPage, pageData)){
           errArg = curPage;
           free(pageData);
           return (_ix->parseIXReturn("IX_ScanIterator::getNextEntry",IX_READ_FAILED));
         }
      }
      //Otherwise, end of the linked list
      else{
         return(IX_EOF);
      }
   }

   if(highSearch != NULL){
      //Check to see if the key is larger, is so then done
      if(_ix->compareKeys(highSearch, ((char *)pageData + curOffset), searchAttr, 0) < 0){
         return(IX_EOF);
      }

      // if equal but not inclusive, done
      if(_ix->compareKeys(highSearch, ((char *)pageData + curOffset), searchAttr, 0) == 0 && !highSearchInc){
         return(IX_EOF);
      }
   }


   //Otherwise copy in value and increment offset
   switch(searchAttr.type){
      case TypeInt:
      case TypeReal:
         memcpy(key, ((char *) pageData + curOffset), sizeof(int));
         memcpy(&rid, ((char *)pageData + curOffset + sizeof(int)), sizeof(RID));
         break;
      case TypeVarChar:
         size_t bytesToCopy = *(int *)((char *) pageData + curOffset);
         memcpy(key, ((char *) pageData + curOffset), bytesToCopy);
         memcpy(&rid, ((char *)pageData + curOffset + bytesToCopy), sizeof(RID));
         curOffset += bytesToCopy;
         break;
   }
   curOffset += sizeof(int) + sizeof(RID);

   return(IX_SUCCESS);
}

//This method should terminate the index scan.
RC IX_ScanIterator::close()
{
   free(pageData);
   return (_ix->parseIXReturn("IX_ScanIterator::close",IX_SUCCESS));
}

/*
* Function: scanInit
* -----------------------------
* Initializes all of the internal variables of a IX_ScanIterator
*
* Arguments: handle, attr, lKey, hKey, lKeyInc, hKeyInc
* handle: the IXFileHandle of the tree to scan
* attr: the information about the keys to search on
* lKey: the lower bound for comparison, may be NULL --> -infinity
* hKey: the upper bound for comparison, may be NULL --> +infinity
* lKeyInc: boolean stating if keys matching lKey are included
* hKeyInc: boolean stating if keys matching hKey are included
*
* Return: RC
* An integer value stating if success (0) or some error (!= 0)
*/
RC IX_ScanIterator::scanInit(IXFileHandle &handle,
      const Attribute &attr,
      const void *lKey,
      const void *hKey,
      bool lKeyInc,
      bool hKeyInc){

      vector<int> trace;
      RID *rid;
      int nextPage;

      _ix = IndexManager::instance();

      //Store variables
      iHandle = handle;
      searchAttr = attr;
      lowSearch = lKey;
      highSearch = hKey;
      lowSearchInc = lKeyInc;
      highSearchInc = hKeyInc;
      curOffset = sizeof(int);
      pageData = calloc(PAGE_SIZE, 1);


      //Find upper bound on pages
      totalPages = handle.getNumberOfPages();

      //NULL lowSearch means start at -infinity
      if(lowSearch == NULL){
         nextPage = 0;

         //Take the leftmost pointer until reach a leaf
         do{
            //Read the page in
            if (handle.ixReadPage(nextPage, pageData)){
              errArg = nextPage;
              free(pageData);
              return (_ix->parseIXReturn("IX_ScanIterator::scanInit",IX_READ_FAILED));
            }
            //Assume node and grab leftmost page pointer
            rid = (RID *) pageData;

            //Save the current page and set nextPage
            curPage = nextPage;
            nextPage = rid->pageNum;
         //Exit if current page is a leaf
      }while(_ix->isLeaf(pageData) < 0);

      //Set the offset to be after the page pointer
      curOffset = sizeof(int);

      //Otherwise there is some defined lower bound to go to
      } else{
         //Find the leaf page to start on
         curPage = _ix->traverseTree(lowSearch, searchAttr, iHandle, trace);
         //If the current leaf page is a valid one, read it
         if (curPage >= 0){
             if (handle.ixReadPage(curPage, pageData)){
                errArg = curPage;
                free(pageData);
                return (_ix->parseIXReturn("IX_ScanIterator::scanInit",IX_READ_FAILED));
             }
         }
         //Otherwise there was an error
         else{
            free(pageData);
            return (_ix->parseIXReturn("IX_ScanIterator::scanInit",IX_PAGE_NOT_FOUND));
         }

         //Find the first key that matches the lower predicate
         //While the lower bound is greater than the key on the page, keep going
         while(_ix->compareKeys(lKey, ((char *) pageData + curOffset), searchAttr, 0) >= 0 &&
            curOffset < _ix->getTotalSize(pageData) - PAGE_DEFAULT + sizeof(int)){

            //If it is equal and inclusive, break here and preserve offset
            if(_ix->compareKeys(lKey, ((char *) pageData + curOffset), searchAttr, 0) == 0){
               if(lKeyInc){
                  break;
               } else{
                  //Increment the offset
                  if(searchAttr.type == TypeVarChar){
                     curOffset += *(int *)((char *) pageData + curOffset);
                  }
                  curOffset += sizeof(int) + sizeof(RID);
                  break;
               }
            }

            //Increment the offset
            if(searchAttr.type == TypeVarChar){
               curOffset += *(int *)((char *) pageData + curOffset);
            }
            curOffset += sizeof(int) + sizeof(RID);

         }
      }

      totalLength = _ix->getTotalSize(pageData);
      //curOffset -= sizeof(int);

      return (_ix->parseIXReturn("IX_ScanIterator::scanInit",IX_SUCCESS));
}



















/***********************************************************************
                           IXFileHandle
***********************************************************************/
IXFileHandle::IXFileHandle()
{
    ixReadPageCounter = 0;
    ixWritePageCounter = 0;
    ixAppendPageCounter = 0;

    _fd = NULL;
}

IXFileHandle::~IXFileHandle()
{
}

RC IXFileHandle::collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount)
{
   readPageCount = ixReadPageCounter;
   writePageCount = ixWritePageCounter;
   appendPageCount = ixAppendPageCounter;

   return IX_SUCCESS;
}

RC IXFileHandle::ixReadPage(PageNum pageNum, void *data)
{
    // If pageNum doesn't exist, error
    if (getNumberOfPages() < pageNum)
        return FH_PAGE_DN_EXIST;

    // Try to seek to the specified page
    if (fseek(_fd, PAGE_SIZE * pageNum, SEEK_SET))
        return FH_SEEK_FAILED;

    // Try to read the specified page
    if (fread(data, 1, PAGE_SIZE, _fd) != PAGE_SIZE)
        return FH_READ_FAILED;

    ixReadPageCounter++;
    return SUCCESS;
}


RC IXFileHandle::ixWritePage(PageNum pageNum, const void *data)
{
    // Check if the page exists
    if (getNumberOfPages() < pageNum)
        return FH_PAGE_DN_EXIST;

    // Seek to the start of the page
    if (fseek(_fd, PAGE_SIZE * pageNum, SEEK_SET))
        return FH_SEEK_FAILED;

    // Write the page
    if (fwrite(data, 1, PAGE_SIZE, _fd) == PAGE_SIZE)
    {
        // Immediately commit changes to disk
        fflush(_fd);
        ixWritePageCounter++;
        return SUCCESS;
    }

    return FH_WRITE_FAILED;
}

RC IXFileHandle::ixAppendPage(const void *data)
{
    // Seek to the end of the file
    if (fseek(_fd, 0, SEEK_END))
        return FH_SEEK_FAILED;

    // Write the new page
    if (fwrite(data, 1, PAGE_SIZE, _fd) == PAGE_SIZE)
    {
        fflush(_fd);
        ixAppendPageCounter++;
        return SUCCESS;
    }
    return FH_WRITE_FAILED;
}

int IXFileHandle::getNumberOfPages()
{
    // Use stat to get the file size
    struct stat sb;
    if (fstat(fileno(_fd), &sb) != 0)
        // On error, return 0
        return 0;
    // Filesize is always PAGE_SIZE * number of pages
    return sb.st_size / PAGE_SIZE;
}

void IXFileHandle::setfd(FILE *fd)
{
    _fd = fd;
}

FILE *IXFileHandle::getfd()
{
    return _fd;
}
