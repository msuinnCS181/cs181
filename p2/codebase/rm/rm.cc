
#include "rm.h"
#include <iostream>

RelationManager* RelationManager::_rm = 0;

RelationManager* RelationManager::instance()
{
    if(!_rm)
        _rm = new RelationManager();

    return _rm;
}

//Constructor
RelationManager::RelationManager()
{
}

//Destructor
RelationManager::~RelationManager()
{
}

//This method creates two system catalog tables, Tables and Columns.
//If they already exist, return an error. The actual files for these two
// tables should be created, and tuples describing themselves should be
//inserted into these tables as shown earlier in the catalog section.
RC RelationManager::createCatalog()
{
   int retVal;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();
   FileHandle tablesHandle;
   FileHandle columnsHandle;
   Attribute field;

   //Create the Tables file
   retVal = rbfm->createFile("Tables");
   if(retVal != 0){
      return(retVal);
   }

   //Create the Columns file
   retVal = rbfm->createFile("Columns");
   if(retVal != 0){
      return(retVal);
   }

   //Setup global table ID map
   TableIDs.clear();
   TableIDs.insert(pair<int, string>(TABLESID, "Tables"));
   TableIDs.insert(pair<int, string>(COLUMNSID, "Columns"));

   TableRIDs.insert(pair<int, vector<RID>>(TABLESID, vector<RID>()));
   TableRIDs.insert(pair<int, vector<RID>>(COLUMNSID, vector<RID>()));

   //Setup the global recordDescriptor for Tables
   field.name = "table-id";
   field.type = TypeInt;
   field.length = sizeof(int);
   TablesRecordDescriptor.push_back(field);

   field.name = "table-name";
   field.type = TypeVarChar;
   field.length = 50;
   TablesRecordDescriptor.push_back(field);

   field.name = "file-name";
   field.type = TypeVarChar;
   field.length = 50;
   TablesRecordDescriptor.push_back(field);

   //Setup the global recordDescriptor for Columns
   field.name = "table-id";
   field.type = TypeInt;
   field.length = sizeof(int);
   ColumnsRecordDescriptor.push_back(field);

   field.name = "column-name";
   field.type = TypeVarChar;
   field.length = 50;
   ColumnsRecordDescriptor.push_back(field);

   field.name = "column-type";
   field.type = TypeInt;
   field.length = sizeof(int);
   ColumnsRecordDescriptor.push_back(field);

   field.name = "column-length";
   field.type = TypeInt;
   field.length = sizeof(int);
   ColumnsRecordDescriptor.push_back(field);

   field.name = "column-position";
   field.type = TypeInt;
   field.length = sizeof(int);
   ColumnsRecordDescriptor.push_back(field);

   //Insert the Tables tuple into Tables
   retVal = insertToTables(TABLESID, "Tables", "Tables");
   if(retVal != 0){
      return(retVal);
   }

   //Insert the Columns tuple into Tables
   retVal = insertToTables(COLUMNSID, "Columns", "Columns");
   if(retVal != 0){
      return(retVal);
   }

   //Insert the table-id for Tables tuple into Columns
   retVal = insertToColumns(TABLESID, "table-id", TypeInt, sizeof(int), 1);
   if(retVal != 0){
      return(retVal);
   }

   //Insert the table-name for Tables tuple into Columns
   retVal = insertToColumns(TABLESID, "table-name", TypeVarChar, 50, 2);
   if(retVal != 0){
      return(retVal);
   }

   //Insert the table-name for Tables tuple into Columns
   retVal = insertToColumns(TABLESID, "file-name", TypeVarChar, 50, 3);
   if(retVal != 0){
      return(retVal);
   }

   //Insert the table-id for Columns tuple into Columns
   retVal = insertToColumns(COLUMNSID, "table-id", TypeInt, sizeof(int), 1);
   if(retVal != 0){
      return(retVal);
   }

   //Insert the column-name for Columns tuple into Columns
   retVal = insertToColumns(COLUMNSID, "column-name", TypeVarChar, 50, 2);
   if(retVal != 0){
      return(retVal);
   }

   //Insert the column-type for Columns tuple into Columns
   retVal = insertToColumns(COLUMNSID, "column-type", TypeInt, sizeof(int), 3);
   if(retVal != 0){
      return(retVal);
   }

   //Insert the column-length for Columns tuple into Columns
   retVal = insertToColumns(COLUMNSID, "column-length", TypeInt, sizeof(int), 4);
   if(retVal != 0){
      return(retVal);
   }

   //Insert the column-position for Columns tuple into Columns
   retVal = insertToColumns(COLUMNSID, "column-position", TypeInt, sizeof(int), 5);
   if(retVal != 0){
      return(retVal);
   }


   return(0);
}

//This method deletes the system catalog tables. The actual files for
//these two tables should be deleted. It will return an error if the
//system catalog does not exist.
RC RelationManager::deleteCatalog()
{
   int retVal;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();

   retVal = rbfm->destroyFile("Tables");
   if(retVal != 0){
      return(retVal);
   }
   return(rbfm->destroyFile("Columns"));
}

//This method creates a table called tableName with a vector of
//attributes (attrs). The actual RBF file for this table should be
//created. This method should return an error if the table already exists.
RC RelationManager::createTable(const string &tableName, const vector<Attribute> &attrs)
{
   int retVal;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();
   FileHandle tablesHandle;
   FileHandle columnsHandle;
   int i;

   //Try to open the Tables file
   retVal = rbfm->openFile("Tables", tablesHandle);
   if(retVal != 0){
      return(retVal);
   }

   //Try to open the Columns file
   retVal = rbfm->openFile("Columns", columnsHandle);
   if(retVal != 0){
      return(retVal);
   }

   //Create the new table and file
   retVal = rbfm->createFile(tableName);
   if(retVal != 0){
      return(retVal);
   }

   retVal = insertToTables(nextTableID, tableName, tableName);
   if(retVal != 0){
      return(retVal);
   }

   for(i = 0; i < attrs.size(); i++){
      retVal = insertToColumns(nextTableID, attrs.at(i).name,
         attrs.at(i).type, attrs.at(i).length, i + 1);
      if(retVal != 0){
         return(retVal);
      }
   }

   //Update map
   TableIDs.insert(pair<int, string>(nextTableID, tableName));

   TableRIDs.insert(pair<int, vector<RID>>(nextTableID, vector<RID>()));


   //Increment the table ID var
   nextTableID++;
   return(0);
}

//This method deletes a table having the given tableName. The actual RBF
// file for this table should be deleted. This method should return an
//error if the table does not exist.
RC RelationManager::deleteTable(const string &tableName)
{
   int retVal;
   FileHandle tablesHandle;
   FileHandle columnsHandle;
   FileHandle toDelete;
   int tableID;
   int returnedTableID;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();
   vector<RID> rids;

   //Cannot query system tables
   if(tableName == "Tables" || tableName == "Columns"){
      return(-1);
   }

   //Try to open the Tables file
   retVal = rbfm->openFile("Tables", tablesHandle);
   if(retVal != 0){
      return(retVal);
   }

   //Try to open the Columns file
   retVal = rbfm->openFile("Columns", columnsHandle);
   if(retVal != 0){
      return(retVal);
   }

   //Try to open the designated file
   retVal = rbfm->openFile(tableName, toDelete);
   if(retVal != 0){
      return(retVal);
   }

   //Try to delete the designated file
   retVal = rbfm->destroyFile(tableName);
   if(retVal != 0){
      return(retVal);
   }

   //Find matching table id
   tableID = getTableID(tableName);

   //Look through Tables file and delete and tuple that matches
   rids = TableRIDs.at(TABLESID);
   for(auto itr = rids.begin(); itr != rids.end(); ++itr){
      //Get the table-id attribute
      rbfm->readAttribute(tablesHandle, TablesRecordDescriptor, *itr,
         "table-id", &returnedTableID);
      //If it matches delete record and entry in vector
      if(tableID == returnedTableID){
         retVal = rbfm->deleteRecord(tablesHandle,
            TablesRecordDescriptor, *itr);
         if(retVal != 0){
            return(retVal);
         }
         itr = rids.erase(itr);
      }
   }

   //Look through Columns file and delete and tuple that matches
   rids = TableRIDs.at(COLUMNSID);
   for(auto itr = rids.begin(); itr != rids.end(); ++itr){
      //Get the table-id attribute
      rbfm->readAttribute(columnsHandle, ColumnsRecordDescriptor, *itr,
         "table-id", &returnedTableID);
      //If it matches delete record and entry in vector
      if(tableID == returnedTableID){
         retVal = rbfm->deleteRecord(columnsHandle,
            ColumnsRecordDescriptor, *itr);
         if(retVal != 0){
            return(retVal);
         }
         itr = rids.erase(itr);
      }
   }

   return(0);
}

//This method gets the attributes (attrs) of a table called tableName by
//looking in the catalog tables. This method should return an error if
//the table does not exist.
RC RelationManager::getAttributes(const string &tableName, vector<Attribute> &attrs)
{
   int tableID = 0;
   int retVal;
   FileHandle handle;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();
   void * fieldData;
   int i;
   vector<RID> rids;
   int offset = 0;
   Attribute attr;
   size_t nameLength;
   int retID;

   //Cannot query system tables
   if(tableName == "Tables" || tableName == "Columns"){
      return(-1);
   }

   //Open up the Columns file
   retVal = rbfm->openFile("Columns", handle);
   if(retVal != 0){
      return(retVal);
   }

   //Find the matching tableID and -1 indicates error
   tableID = getTableID(tableName);
   if(tableID < 0){
      return(-1);
   }

   //Assume that each record is less than a page
   fieldData = malloc(PAGE_SIZE);

   //For each RID with the matching tableID in the Columns table
   rids = TableRIDs.at(COLUMNSID);
   for(i = 0; i < rids.size(); i++){
      //Reset offset
      offset = 0;

      //Read in each one of the
      retVal = rbfm->readRecord(handle, ColumnsRecordDescriptor,
         rids.at(i), fieldData);
      if(retVal != 0){
         free(fieldData);
         return(retVal);
      }

      //Otherwise assume that data is good, skip NULL fields
      offset += ceil((float) ColumnsRecordDescriptor.size()/ 8);

      retID = *(int *)((char *) fieldData + offset);
      if(tableID != retID){
         continue;
      }

      offset += sizeof(int);

      //Set the name field
      nameLength = *(int *)((char *) fieldData + offset);
      attr.name = string(((char *) fieldData + offset + sizeof(int)),
         nameLength);
      offset += nameLength + sizeof(int);

      //Set the type
      attr.type = (AttrType)*((int *) fieldData + offset);
      offset += sizeof(int);

      //Set the length
      attr.length = *((int *) fieldData + offset);
      offset += sizeof(int);

      //Push this attr to the list
      attrs.push_back(attr);
   }

   free(fieldData);
   return(0);
}

//This method inserts a tuple into a table called tableName.
//This method should return an error if the table does not exist.
//However, you can assume that the rest of the input is always correct
//and error-free. That is, you do not need to check if the input tuple
//has the right number of attributes and/or if the attribute types
//match. Since there can be NULL values in one or more attributes, the
//first part in *data contains n bytes to pass the null information
//about each attributes. For details, see insertRecord() in the Project
//1 Description.
RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid)
{
   FileHandle handle;
   int retVal;
   int tableID;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();
   vector<Attribute> recDesc;

   //Cannot query system tables
   if(tableName == "Tables" || tableName == "Columns"){
      return(-1);
   }

   //Try to open the file
   retVal = rbfm->openFile(tableName, handle);
   if(retVal != 0){
      return(retVal);
   }

   //Fill out the record descriptor for the table
   retVal = getAttributes(tableName, recDesc);
   if(retVal != 0){
      return(retVal);
   }

   //Try and insert the data
   retVal = rbfm->insertRecord(handle, recDesc, data, rid);
   if(retVal != 0){
      return(retVal);
   }

   //Find the matching tableID and -1 indicates error
   tableID = getTableID(tableName);
   if(tableID < 0){
      return(-1);
   }

   //Push the returned RID to the corresponding ID, vec<RID> pair
   TableRIDs.at(tableID).push_back(rid);

   return(0);
}

//This method deletes a tuple with a given rid. This method should
//return an error if the table does not exist, or if there is no record
//in the table with the corresponding rid. Also, each time a tuple is
//deleted, you will need to compact the underlying page. That is, keep
//the free space together in the middle of the page -- the slot table
//will be at one end, the record data area will be at the other end, and
// the free space should be in the middle.
RC RelationManager::deleteTuple(const string &tableName, const RID &rid)
{
   FileHandle handle;
   int retVal;
   int tableID;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();
   vector<Attribute> recDesc;
   vector<RID> rids;

   //Cannot query system tables
   if(tableName == "Tables" || tableName == "Columns"){
      return(-1);
   }

   //Try to open the file
   retVal = rbfm->openFile(tableName, handle);
   if(retVal != 0){
      return(retVal);
   }

   //Fill out the record descriptor for the table
   retVal = getAttributes(tableName, recDesc);
   if(retVal != 0){
      return(retVal);
   }

   //Try to delete the record
   retVal = rbfm->deleteRecord(handle, recDesc, rid);
   if(retVal != 0){
      return(retVal);
   }

   //Find the matching tableID and -1 indicates error
   tableID = getTableID(tableName);
   if(tableID < 0){
      return(-1);
   }

   //Delete from the RID mapping
   rids = TableRIDs.at(tableID);
   for(auto itr = rids.begin(); itr != rids.end(); ++itr){
      if((*itr).pageNum == rid.pageNum && (*itr).slotNum == rid.slotNum){
         itr = rids.erase(itr);
      }
   }


   return(0);
}

//This method updates a tuple identified by a given rid. This method
//should return an error if the table does not exist, or if there is no
//record in the table with the corresponding rid.
//Note: if the tuple grows (i.e., the size of the tuple increases) and
//there is no space in the page to store the tuple (after the update),
//then the tuple is migrated to a new page with enough free space. Since
// you will implement an index structure (e.g., B-tree) in Project 3,
//tuples must be permanently identified by their rids, so when they
//migrate, you must leave a “forwarding address” behind identifying the
//new location of the tuple. Also, each time a tuple is updated to
//become smaller, you need to compact the underlying page. That is, keep
// the free space in the middle of the page -- the slot table will be at
// one end, the tuple data area will be at the other end, and the free
//space should be in the middle. Again, the structure for *data is the
//same as for insertRecord().
RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid)
{
   FileHandle handle;
   int retVal;
   int tableID;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();
   vector<Attribute> recDesc;
   vector<RID> rids;
   vector<RID>::iterator itr;

   //Cannot query system tables
   if(tableName == "Tables" || tableName == "Columns"){
      return(-1);
   }

   //Try to open the file
   retVal = rbfm->openFile(tableName, handle);
   if(retVal != 0){
      return(retVal);
   }

   //Fill out the record descriptor for the table
   retVal = getAttributes(tableName, recDesc);
   if(retVal != 0){
      return(retVal);
   }

   //Find the matching tableID and -1 indicates error
   tableID = getTableID(tableName);
   if(tableID < 0){
      return(-1);
   }

   //Try to find the matching entry in mapping
   rids = TableRIDs.at(tableID);
   for(itr = rids.begin(); itr != rids.end(); ++itr){
      if((*itr).pageNum == rid.pageNum && (*itr).slotNum == rid.slotNum){
         break;
      }
   }

   //Try and update the data
   retVal = rbfm->updateRecord(handle, recDesc, data, rid);
   if(retVal != 0){
      return(retVal);
   }

   //Update rids vector
   (*itr).pageNum = rid.pageNum;
   (*itr).slotNum = rid.slotNum;



   return(0);
}

//This method reads a tuple identified by a given rid. The structure for
// *data is the same as for insertRecord().This method should return an
//error if the table does not exist, or if there is no record in the
//table with the corresponding rid.
RC RelationManager::readTuple(const string &tableName, const RID &rid, void *data)
{
   FileHandle handle;
   int retVal;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();
   vector<Attribute> recDesc;

   //Cannot query system tables
   if(tableName == "Tables" || tableName == "Columns"){
      return(-1);
   }

   //Try to open the file
   retVal = rbfm->openFile(tableName, handle);
   if(retVal != 0){
      return(retVal);
   }

   //Fill out the record descriptor for the table
   retVal = getAttributes(tableName, recDesc);
   if(retVal != 0){
      return(retVal);
   }

   //Try to read the record
   retVal = rbfm->readRecord(handle, recDesc, rid, data);
   if(retVal != 0){
      return(retVal);
   }


   return(0);
}

//This method mainly exists for debugging purposes. This method prints
//the tuple whose data is passed into this method. The structure for
//*data is the same as for insertRecord(). For details, refer to
//printRecord() in the Project 1 Description.
RC RelationManager::printTuple(const vector<Attribute> &attrs, const void *data)
{
   size_t offset = 0;
   size_t nullSize = ceil((float) attrs.size() / 8);
   char nulls[nullSize];
   int i;
   size_t strLen;

   //Copy null bytes into nulls
   memcpy(&nulls[0], data, nullSize);

   //Skip nulls
   offset += nullSize;
   cout << "NULL size " << nullSize << endl;

   //Go through each attribute
   for(i = 0; i < attrs.size(); i++){
      cout <<"Field Name: "<< attrs.at(i).name << "\t\tData: ";
      switch(attrs.at(i).type){
         case TypeInt:
            cout << (*(int *)((char *) data + offset));
            offset += sizeof(int);
            break;
         case TypeReal:
            cout << (*(float *)((char *) data + offset));
            offset += sizeof(int);
            break;
         case TypeVarChar:
            strLen = *(int *)((char *) data + offset);
            cout << string((char *) data + offset, strLen);
            offset += strLen + sizeof(int);
            break;
      }
      cout << endl;
   }


   return(0);
}

//This method mainly exists for debugging purposes. This method reads a
//specific attribute of a tuple identified by a given rid. The structure
// for *data is the same as for insertRecord(). That is, a
//null-indicator will be placed in the beginning of *data. However, for
//this function, since it returns a value for just one attribute,
//exactly one byte of null-indicators should be returned, not a set of
//the null-indicators for all of the tuple's attributes. This method
//should return an error if the table does not exist, or if there is no
//record in the table with the corresponding rid, or if there is no
//attribute with the specified attribute name in the table.
RC RelationManager::readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data)
{
   FileHandle handle;
   int retVal;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();
   vector<Attribute> recDesc;

   //Cannot query system tables
   if(tableName == "Tables" || tableName == "Columns"){
      return(-1);
   }

   //Try to open the file
   retVal = rbfm->openFile(tableName, handle);
   if(retVal != 0){
      return(retVal);
   }

   //Fill out the record descriptor for the table
   retVal = getAttributes(tableName, recDesc);
   if(retVal != 0){
      return(retVal);
   }

   //Try to read the attribute
   retVal = rbfm->readAttribute(handle, recDesc, rid, attributeName, data);
   if(retVal != 0){
      return(retVal);
   }


   return(0);
}

//This method scans the table called tableName. That is, it sequentially
//reads all of the tuples in the table. This method returns an iterator
//called rm_ScanIterator to allow the caller to go through the records
//in the table one by one. A scan has a basic query associated with it,
//e.g., it consists of a list of attributes to project into the answer,
//as well as a simple predicate on an attribute and constant (such as
//“Sal > 40000”). Note: the RBFM_ScanIterator should not cache the scan
//result in memory. In fact, your code should be looking only at one
//(or a few) page(s) of data at a time when getNextTuple() is called. In
// this project, let the OS do all the memory management work for you.
//As usual, this method should return an error if the table does not
//exist, or if the attributes named in the predicate or the projection
//don’t exist in the table.
RC RelationManager::scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,
      const void *value,
      const vector<string> &attributeNames,
      RM_ScanIterator &rm_ScanIterator)
{
   FileHandle handle;
   int retVal;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();
   vector<Attribute> recDesc;
   RBFM_ScanIterator uselessThing;

   //Cannot query system tables
   if(tableName == "Tables" || tableName == "Columns"){
      return(-1);
   }

   //Try to open the file
   retVal = rbfm->openFile(tableName, handle);
   if(retVal != 0){
      return(retVal);
   }

   //Fill out the record descriptor for the table
   retVal = getAttributes(tableName, recDesc);
   if(retVal != 0){
      return(retVal);
   }

   //Try to read the attribute
   retVal = rbfm->scan(handle, recDesc, conditionAttribute, compOp,
      value, attributeNames, uselessThing);
   if(retVal != 0){
      return(retVal);
   }


   return(0);
}

void RelationManager::createRecord(vector<RecDataAndInfo> list, void * rec){
   int offset = 0;
   int i;
   int iData;
   float fData;
   RecDataAndInfo field;
   size_t nullLength = ceil((float)list.size()/ 8);
   int strLen;

   //Assume all fields are not null
   memset(rec, 0, nullLength);
   offset += nullLength;



   //Go through each field and set rec accordingly
   for(i = 0; i < list.size(); i++){
      field = list.at(i);
      //If the length is NULL, set NULL bit and continue
      if(field.length == 0){
         //i / 8 is 0 for i < 8, 1 for 8 < i < 16 and so on
         //0x80 >> (i % 8) is ith bit from the left as 1, else 0
         ((char *)rec)[i / 8] |= (0x80 >> (i % 8));
         continue;
      }

      cout << field.data << ", " << field.length << endl;

      //Otherwise there is some data to write that depends on the type
      switch (field.attr.type){
         case TypeInt:
            iData = stoi(field.data);
            memcpy((char *) rec + offset, &iData, sizeof(int));
            offset += sizeof(int);
            break;
         case TypeReal:
            fData = stof(field.data);
            memcpy((char *) rec + offset, &fData, sizeof(int));
            offset += sizeof(int);
            break;
         case TypeVarChar:
            strLen = field.length - sizeof(int);
            memcpy((char *) rec + offset, &strLen, sizeof(int));
            offset += sizeof(int);
            memcpy((char *) rec + offset, (char *)&field.data + 1, strLen);
            offset += strLen;
            break;
      }
   }
}

void RelationManager::populateRecData(Attribute attrs,
   string data, RecDataAndInfo &rec){

   rec.attr.name = attrs.name;
   rec.attr.type = attrs.type;
   rec.attr.length = attrs.length;
   rec.data = data;

   switch (rec.attr.type) {
      case TypeInt:
      case TypeReal:
         rec.length = sizeof(int);
         break;
      case TypeVarChar:
         rec.length = sizeof(int) + rec.data.length();
         break;
   }
}

int RelationManager::insertToTables(int id, string tabName,
      string fileName){

   RecDataAndInfo entry;
   vector<RecDataAndInfo> entryList;
   size_t recSize = 0;
   void * record;
   RID rid;
   int retVal;
   FileHandle handle;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();

   //Try to open Tables file
   retVal = rbfm->openFile("Tables", handle);
   if(retVal != 0){
      return(retVal);
   }

   //Create the vector to add to the Tables table
   //Set the table ID to be the number of tables
   populateRecData(TablesRecordDescriptor.at(0), to_string(id), entry);
   entryList.push_back(entry);
   recSize += entry.length;

   //Set the table name to be the passed in name
   populateRecData(TablesRecordDescriptor.at(1), tabName, entry);
   entryList.push_back(entry);
   recSize += entry.length;

   //Set the file name to be the passed in name
   populateRecData(TablesRecordDescriptor.at(2), fileName, entry);
   entryList.push_back(entry);
   recSize += entry.length;

   //Malloc buffer for record
   record = malloc(recSize);

   //Create the record to add and add it to the table
   createRecord(entryList, record);

   retVal = rbfm->insertRecord(handle, TablesRecordDescriptor, record,
      rid);
   free(record);
   if(retVal != 0){
      return(-1);
   }

   //Push the returned RID to the corresponding ID, vec<RID> pair
   TableRIDs.at(TABLESID).push_back(rid);

   return(0);
}

int RelationManager::insertToColumns(int id, string colName, int type,
   int colLen, int colPos){

   RecDataAndInfo entry;
   vector<RecDataAndInfo> entryList;
   size_t recSize = 0;
   void * record;
   RID rid;
   int retVal;
   FileHandle handle;
   RecordBasedFileManager* rbfm = RecordBasedFileManager::instance();

   //Try to open Tables file
   retVal = rbfm->openFile("Columns", handle);
   if(retVal != 0){
      return(retVal);
   }

   //Set the table ID to be the numTables
   populateRecData(ColumnsRecordDescriptor.at(0), to_string(id), entry);
   entryList.push_back(entry);
   recSize += entry.length;

   //Set the column name to be the attr name
   populateRecData(ColumnsRecordDescriptor.at(1), colName, entry);
   entryList.push_back(entry);
   recSize += entry.length;

   //Set the column type to be the attr type
   populateRecData(ColumnsRecordDescriptor.at(2), to_string(type),
      entry);
   entryList.push_back(entry);
   recSize += entry.length;

   //Set the length to be the attr length
   populateRecData(ColumnsRecordDescriptor.at(3), to_string(colLen),
      entry);
   entryList.push_back(entry);
   recSize += entry.length;

   //Set the column position to be 1 + i since i starts at zero and
   //positions start at 1
   populateRecData(ColumnsRecordDescriptor.at(4), to_string(colPos),
      entry);
   entryList.push_back(entry);
   recSize += entry.length;

   //Malloc buffer for record
   record = malloc(recSize);

   //Create the record to add and add it to the table
   createRecord(entryList, record);
   retVal = rbfm->insertRecord(handle, ColumnsRecordDescriptor, record,
      rid);
   free(record);
   if(retVal != 0){
      return(-1);
   }

   //Push the returned RID to the corresponding ID, vec<RID> pair
   TableRIDs.at(COLUMNSID).push_back(rid);

   return(0);
}

int RelationManager::getTableID(string tableName){
   int tableID = -1;
   //Find the ID of the requested table from the map
   for(auto pair : TableIDs){
      if(pair.second == tableName){
         tableID = pair.first;
         break;
      }
   }

   return(tableID);
}
