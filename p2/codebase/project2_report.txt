1. Basic information
Team Number : 12
Student ID# of Submitter:
Name of Submitter: Michael Suinn
ID#s and Names for others on the Team: Deshawn Rivers (#1334526), Catherine Shen (#)


2. Meta-data
- Show your meta-data design (Tables and Columns table) and information about each column.

Our meta-data design obviously consists of the catalog that is used to show which files that are stored in the database.  When the catalog is created in the createCatalog method, we create the files associated with the default Tables and Columns tables.  We then use our TableIDs map of the form <int,string>, and insert our new Tables and Column table into our newly cleared map.  This map is updated whenever a new table is created.  We setup the field lengths for the Table and Columns table, corresponding to the information given in the pdf.  These field lengths are then pushed back into our two vectors; TablesRecordDescriptor and ColumnsRecordDescriptor.  These represent the sizes of the columns for our Tables and Columns tables.  We then call insertToColumns and insertToTables helper functions that we implemented to handle the proper inserting into the the two tables.  InsertRecord is also called from within these two helper functions.  

3. Internal Record Format
- Show your record format design and describe how your design satisfies O(1) field access. If not, just mention that your team hasn't implemented this feature.
- Describe how you store a VarChar field.
- Describe how you deal with an update and delete.

We store a VarChar field in a record based on its specific size.  Namely, each of its characters takes up a byte, and a integer at the beginning of the string, usually four bytes long, that indicates how long the string is.  Thus we store based on the sizeof(int) and size of the actual string of characters.  We did not implement the updates and deleting of attributes, but the updating and deleting of records is described in the Page Format section.


4. Page Format
- Show your page format design.
- Describe how you deal with an update and delete.

For our page format, we have the Slot Directory implemented as being at the top of the page.  The internal structure of the Slot Directory remains the same as presented in the slides; with the null indicator and the field count at the beginning.  As for updates, we first check to see if the record is on the current page. If we encounter a forwarding address, we make a recursive call to updateRecord with the new page as an argument.  If the record is the same size as before the update, then we can just write over it.  If it is now smaller, then we need to move its offset location, and call our moveFollowingRecords helper function.  If the record is bigger, we may have to use a forwarding address and move it if there is not enough free space on the page.  Either way, our moveFollowingRecords function is called to move the other records on the page.  DeleteRecord is handled in a similar way. Namely, if we encounter a forwarding address, meaning the offset of the record is negative, then we make a recursive call to DeleteRecord, with the new RID information.  We then move the other records to overwrite the cleared memory spot using memset.  The records are moved using moveFollowingRecords.  The redundant data is then cleared out.  Note that for forwarding addresses, we use the negative offset for the new page number and the length as our new slot number.  Pages are also appended at the end of a file as implemented in the appendPage method in the pfm.h file.    


5. File Format
- Show your file format design

New files are created when the catalog is created; the Table and the Column file.  Also, we create new files when there is a new table that is added.  These files are created using the rbfm object, which in turn, uses the ppm class to create the file.  The files are created with the name corresponding to the tableName parameter that was given in the createTable method of rm.h.  The files are then handled using the FileHandle class within ppm.h.


6. Implementation Detail
- Other implementation details goes here.

We stuck to the outline of the project given to us in the pdf, but we edited the rbfm.h and rm.h files to add some helper functions and structs.  These additions help our code run smoother.


7. Other (optional)
- Freely use this section to tell us about things that are related to the project 2, but not related to the other sections (optional)
