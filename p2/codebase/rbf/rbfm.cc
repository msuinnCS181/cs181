#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <string>

#include "rbfm.h"

//initializing the _rbf_manager instance to null.
RecordBasedFileManager* RecordBasedFileManager::_rbf_manager = NULL;
//doing the same to the _pf_manager since they have not been used yet.
PagedFileManager *RecordBasedFileManager::_pf_manager = NULL;

//Enforcing the singleton property of the _rbf_manager
RecordBasedFileManager* RecordBasedFileManager::instance()
{
    //check to see if the _rbf_manager is not null, if it is null, we can create a new
    if(!_rbf_manager)
        //creates a RecordBasedFileManager object called _rbf_manager when we do not have one created yet.
        //Thus the contructor of the RecordBasedFileManager class is called, and through that an instance of the PagedFileManager class is created.
        //_rbf_manger is a pointer to the only RecordBasedFile Manager object.
        _rbf_manager = new RecordBasedFileManager();

    return _rbf_manager;
}

RecordBasedFileManager::RecordBasedFileManager()
{
    // Initialize the internal PagedFileManager instance.  Goes to the method defined in the PagedFileManager class as instance.  We included this through inheritance from rbfm.h.
    //_pf_manger is a pointer to the PagedFileManager object
    _pf_manager = PagedFileManager::instance();
}

//Empty descructor for the RecordBasedFile Manager.
RecordBasedFileManager::~RecordBasedFileManager()
{
}

RC RecordBasedFileManager::createFile(const string &fileName)
{
    // Creating a new paged file.
    if (_pf_manager->createFile(fileName))
        return RBFM_CREATE_FAILED;

    // Setting up the first page.
    void * firstPageData = calloc(PAGE_SIZE, 1);
    if (firstPageData == NULL)
        return RBFM_MALLOC_FAILED;
    newRecordBasedPage(firstPageData);

    // Adds the first record based page.
    FileHandle handle;
    if (_pf_manager->openFile(fileName.c_str(), handle))
        return RBFM_OPEN_FAILED;
    if (handle.appendPage(firstPageData))
        return RBFM_APPEND_FAILED;
    _pf_manager->closeFile(handle);

    free(firstPageData);

    return SUCCESS;
}

RC RecordBasedFileManager::destroyFile(const string &fileName)
{
    return _pf_manager->destroyFile(fileName);
}

RC RecordBasedFileManager::openFile(const string &fileName, FileHandle &fileHandle)
{
    return _pf_manager->openFile(fileName.c_str(), fileHandle);
}

RC RecordBasedFileManager::closeFile(FileHandle &fileHandle)
{
    return _pf_manager->closeFile(fileHandle);
}

RC RecordBasedFileManager::insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid)
{
    // Gets the size of the record.
    unsigned recordSize = getRecordSize(recordDescriptor, data);

    // Cycles through pages looking for enough free space for the new entry.
    void *pageData = malloc(PAGE_SIZE);
    if (pageData == NULL)
        return RBFM_MALLOC_FAILED;
    bool pageFound = false;
    unsigned i;
    unsigned numPages = fileHandle.getNumberOfPages();
    for (i = 0; i < numPages; i++)
    {
        if (fileHandle.readPage(i, pageData))
            return RBFM_READ_FAILED;

        // When we find a page with enough space (accounting also for the size that will be added to the slot directory), we stop the loop.
        if (getPageFreeSpaceSize(pageData) >= sizeof(SlotDirectoryRecordEntry) + recordSize)
        {
            pageFound = true;
            break;
        }
    }

    // If we can't find a page with enough space, we create a new one
    if(!pageFound)
    {
        newRecordBasedPage(pageData);
    }

    SlotDirectoryHeader slotHeader = getSlotDirectoryHeader(pageData);

    // Setting the return RID.
    rid.pageNum = i;
    rid.slotNum = slotHeader.recordEntriesNumber;

    // Adding the new record reference in the slot directory.
    SlotDirectoryRecordEntry newRecordEntry;
    newRecordEntry.length = recordSize;
    newRecordEntry.offset = slotHeader.freeSpaceOffset - recordSize;
    setSlotDirectoryRecordEntry(pageData, rid.slotNum, newRecordEntry);

    // Updating the slot directory header.
    slotHeader.freeSpaceOffset = newRecordEntry.offset;
    slotHeader.recordEntriesNumber += 1;
    setSlotDirectoryHeader(pageData, slotHeader);

    // Adding the record data.
    setRecordAtOffset (pageData, newRecordEntry.offset, recordDescriptor, data);

    // Writing the page to disk.
    if (pageFound)
    {
        if (fileHandle.writePage(i, pageData))
            return RBFM_WRITE_FAILED;
    }
    else
    {
        if (fileHandle.appendPage(pageData))
            return RBFM_APPEND_FAILED;
    }

    free(pageData);
    return SUCCESS;
}

RC RecordBasedFileManager::readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data)
{
    // Retrieve the specific page
    void * pageData = malloc(PAGE_SIZE);
    if (fileHandle.readPage(rid.pageNum, pageData))
        return RBFM_READ_FAILED;

    // Checks if the specific slot id exists in the page
    SlotDirectoryHeader slotHeader = getSlotDirectoryHeader(pageData);
    if(slotHeader.recordEntriesNumber < rid.slotNum)
        return RBFM_SLOT_DN_EXIST;

    // Gets the slot directory record entry data
    SlotDirectoryRecordEntry recordEntry = getSlotDirectoryRecordEntry(pageData, rid.slotNum);

    // Retrieve the actual entry data
    getRecordAtOffset(pageData, recordEntry.offset, recordDescriptor, data);

    free(pageData);
    return SUCCESS;
}

RC RecordBasedFileManager::printRecord(const vector<Attribute> &recordDescriptor, const void *data)
{
    // Parse the null indicator into an array
    int nullIndicatorSize = getNullIndicatorSize(recordDescriptor.size());
    char nullIndicator[nullIndicatorSize];
    memset(nullIndicator, 0, nullIndicatorSize);
    memcpy(nullIndicator, data, nullIndicatorSize);

    // We've read in the null indicator, so we can skip past it now
    unsigned offset = nullIndicatorSize;

    cout << "----" << endl;
    for (unsigned i = 0; i < (unsigned) recordDescriptor.size(); i++)
    {
        cout << setw(10) << left << recordDescriptor[i].name << ": ";
        // If the field is null, don't print it
        bool isNull = fieldIsNull(nullIndicator, i);
        if (isNull)
        {
            cout << "NULL" << endl;
            continue;
        }
        switch (recordDescriptor[i].type)
        {
            case TypeInt:
                uint32_t data_integer;
                memcpy(&data_integer, ((char*) data + offset), INT_SIZE);
                offset += INT_SIZE;

                cout << "" << data_integer << endl;
            break;
            case TypeReal:
                float data_real;
                memcpy(&data_real, ((char*) data + offset), REAL_SIZE);
                offset += REAL_SIZE;

                cout << "" << data_real << endl;
            break;
            case TypeVarChar:
                // First VARCHAR_LENGTH_SIZE bytes describe the varchar length
                uint32_t varcharSize;
                memcpy(&varcharSize, ((char*) data + offset), VARCHAR_LENGTH_SIZE);
                offset += VARCHAR_LENGTH_SIZE;

                // Gets the actual string.
                char *data_string = (char*) malloc(varcharSize + 1);
                if (data_string == NULL)
                    return RBFM_MALLOC_FAILED;
                memcpy(data_string, ((char*) data + offset), varcharSize);

                // Adds the string terminator.
                data_string[varcharSize] = '\0';
                offset += varcharSize;

                cout << data_string << endl;
                free(data_string);
            break;
        }
    }
    cout << "----" << endl;

    return SUCCESS;
}

// Private helper methods

// Configures a new record based page, and puts it in "page".
void RecordBasedFileManager::newRecordBasedPage(void * page)
{
    memset(page, 0, PAGE_SIZE);
    // Writes the slot directory header.
    SlotDirectoryHeader slotHeader;
    slotHeader.freeSpaceOffset = PAGE_SIZE;
    slotHeader.recordEntriesNumber = 0;
    setSlotDirectoryHeader(page, slotHeader);
}

SlotDirectoryHeader RecordBasedFileManager::getSlotDirectoryHeader(void * page)
{
    // Getting the slot directory header.
    SlotDirectoryHeader slotHeader;
    memcpy (&slotHeader, page, sizeof(SlotDirectoryHeader));
    return slotHeader;
}

void RecordBasedFileManager::setSlotDirectoryHeader(void * page, SlotDirectoryHeader slotHeader)
{
    // Setting the slot directory header.
    memcpy (page, &slotHeader, sizeof(SlotDirectoryHeader));
}

SlotDirectoryRecordEntry RecordBasedFileManager::getSlotDirectoryRecordEntry(void * page, unsigned recordEntryNumber)
{
    // Getting the slot directory entry data.
    SlotDirectoryRecordEntry recordEntry;
    memcpy  (
            &recordEntry,
            ((char*) page + sizeof(SlotDirectoryHeader) + recordEntryNumber * sizeof(SlotDirectoryRecordEntry)),
            sizeof(SlotDirectoryRecordEntry)
            );

    return recordEntry;
}

void RecordBasedFileManager::setSlotDirectoryRecordEntry(void * page, unsigned recordEntryNumber, SlotDirectoryRecordEntry recordEntry)
{
    // Setting the slot directory entry data.
    memcpy  (
            ((char*) page + sizeof(SlotDirectoryHeader) + recordEntryNumber * sizeof(SlotDirectoryRecordEntry)),
            &recordEntry,
            sizeof(SlotDirectoryRecordEntry)
            );
}

// Computes the free space of a page (function of the free space pointer and the slot directory size).
unsigned RecordBasedFileManager::getPageFreeSpaceSize(void * page)
{
    SlotDirectoryHeader slotHeader = getSlotDirectoryHeader(page);
    return slotHeader.freeSpaceOffset - slotHeader.recordEntriesNumber * sizeof(SlotDirectoryRecordEntry) - sizeof(SlotDirectoryHeader);
}

unsigned RecordBasedFileManager::getRecordSize(const vector<Attribute> &recordDescriptor, const void *data)
{
    // Read in the null indicator
    int nullIndicatorSize = getNullIndicatorSize(recordDescriptor.size());
    char nullIndicator[nullIndicatorSize];
    memset(nullIndicator, 0, nullIndicatorSize);
    memcpy(nullIndicator, (char*) data, nullIndicatorSize);

    // Offset into *data. Start just after null indicator
    unsigned offset = nullIndicatorSize;
    // Running count of size. Initialize to size of header
    unsigned size = sizeof (RecordLength) + (recordDescriptor.size()) * sizeof(ColumnOffset) + nullIndicatorSize;

    for (unsigned i = 0; i < (unsigned) recordDescriptor.size(); i++)
    {
        // Skip null fields
        if (fieldIsNull(nullIndicator, i))
            continue;
        switch (recordDescriptor[i].type)
        {
            case TypeInt:
                size += INT_SIZE;
                offset += INT_SIZE;
            break;
            case TypeReal:
                size += REAL_SIZE;
                offset += REAL_SIZE;
            break;
            case TypeVarChar:
                uint32_t varcharSize;
                // We have to get the size of the VarChar field by reading the integer that precedes the string value itself
                memcpy(&varcharSize, (char*) data + offset, VARCHAR_LENGTH_SIZE);
                size += varcharSize;
                offset += varcharSize + VARCHAR_LENGTH_SIZE;
            break;
        }
    }

    return size;
}

// Calculate actual bytes for nulls-indicator for the given field counts
int RecordBasedFileManager::getNullIndicatorSize(int fieldCount)
{
    return int(ceil((double) fieldCount / CHAR_BIT));
}

bool RecordBasedFileManager::fieldIsNull(char *nullIndicator, int i)
{
    int indicatorIndex = i / CHAR_BIT;
    int indicatorMask  = 1 << (CHAR_BIT - 1 - (i % CHAR_BIT));
    return (nullIndicator[indicatorIndex] & indicatorMask) != 0;
}

void RecordBasedFileManager::setRecordAtOffset(void *page, unsigned offset, const vector<Attribute> &recordDescriptor, const void *data)
{
    // Read in the null indicator
    int nullIndicatorSize = getNullIndicatorSize(recordDescriptor.size());
    char nullIndicator[nullIndicatorSize];
    memset (nullIndicator, 0, nullIndicatorSize);
    memcpy(nullIndicator, (char*) data, nullIndicatorSize);

    // Points to start of record
    char *start = (char*) page + offset;

    // Offset into *data
    unsigned data_offset = nullIndicatorSize;
    // Offset into page header
    unsigned header_offset = 0;

    RecordLength len = recordDescriptor.size();
    memcpy(start + header_offset, &len, sizeof(len));
    header_offset += sizeof(len);

    memcpy(start + header_offset, nullIndicator, nullIndicatorSize);
    header_offset += nullIndicatorSize;

    // Keeps track of the offset of each record
    // Offset is relative to the start of the record and points to the END of a field
    ColumnOffset rec_offset = header_offset + (recordDescriptor.size()) * sizeof(ColumnOffset);

    unsigned i = 0;
    for (i = 0; i < recordDescriptor.size(); i++)
    {
        if (!fieldIsNull(nullIndicator, i))
        {
            // Points to current position in *data
            char *data_start = (char*) data + data_offset;

            // Read in the data for the next column, point rec_offset to end of newly inserted data
            switch (recordDescriptor[i].type)
            {
                case TypeInt:
                    memcpy (start + rec_offset, data_start, INT_SIZE);
                    rec_offset += INT_SIZE;
                    data_offset += INT_SIZE;
                break;
                case TypeReal:
                    memcpy (start + rec_offset, data_start, REAL_SIZE);
                    rec_offset += REAL_SIZE;
                    data_offset += REAL_SIZE;
                break;
                case TypeVarChar:
                    unsigned varcharSize;
                    // We have to get the size of the VarChar field by reading the integer that precedes the string value itself
                    memcpy(&varcharSize, data_start, VARCHAR_LENGTH_SIZE);
                    memcpy(start + rec_offset, data_start + VARCHAR_LENGTH_SIZE, varcharSize);
                    // We also have to account for the overhead given by that integer.
                    rec_offset += varcharSize;
                    data_offset += VARCHAR_LENGTH_SIZE + varcharSize;
                break;
            }
        }
        // Copy offset into record header
        // Offset is relative to the start of the record and points to END of field
        memcpy(start + header_offset, &rec_offset, sizeof(ColumnOffset));
        header_offset += sizeof(ColumnOffset);
    }
}

// Support header size and null indicator. If size is less than recordDescriptor size, then trailing records are null
// Memset null indicator as 1?
void RecordBasedFileManager::getRecordAtOffset(void *page, unsigned offset, const vector<Attribute> &recordDescriptor, void *data)
{
    // Pointer to start of record
    char *start = (char*) page + offset;

    // Allocate space for null indicator. The returned null indicator may be larger than
    // the null indicator in the table has had fields added to it
    int nullIndicatorSize = getNullIndicatorSize(recordDescriptor.size());
    char nullIndicator[nullIndicatorSize];
    memset(nullIndicator, 0, nullIndicatorSize);

    // Get number of columns and size of the null indicator for this record
    RecordLength len = 0;
    memcpy (&len, start, sizeof(RecordLength));
    int recordNullIndicatorSize = getNullIndicatorSize(len);

    // Read in the existing null indicator
    memcpy (nullIndicator, start + sizeof(RecordLength), recordNullIndicatorSize);

    // If this new recordDescriptor has had fields added to it, we set all of the new fields to null
    for (unsigned i = len; i < recordDescriptor.size(); i++)
    {
        int indicatorIndex = (i+1) / CHAR_BIT;
        int indicatorMask  = 1 << (CHAR_BIT - 1 - (i % CHAR_BIT));
        nullIndicator[indicatorIndex] |= indicatorMask;
    }
    // Write out null indicator
    memcpy(data, nullIndicator, nullIndicatorSize);

    // Initialize some offsets
    // rec_offset: points to data in the record. We move this forward as we read data from our record
    unsigned rec_offset = sizeof(RecordLength) + recordNullIndicatorSize + len * sizeof(ColumnOffset);
    // data_offset: points to our current place in the output data. We move this forward as we write data to data.
    unsigned data_offset = nullIndicatorSize;
    // directory_base: points to the start of our directory of indices
    char *directory_base = start + sizeof(RecordLength) + recordNullIndicatorSize;

    for (unsigned i = 0; i < recordDescriptor.size(); i++)
    {
        if (fieldIsNull(nullIndicator, i))
            continue;

        // Grab pointer to end of this column
        ColumnOffset endPointer;
        memcpy(&endPointer, directory_base + i * sizeof(ColumnOffset), sizeof(ColumnOffset));

        // rec_offset keeps track of start of column, so end-start = total size
        uint32_t fieldSize = endPointer - rec_offset;

        // Special case for varchar, we must give data the size of varchar first
        if (recordDescriptor[i].type == TypeVarChar)
        {
            memcpy((char*) data + data_offset, &fieldSize, VARCHAR_LENGTH_SIZE);
            data_offset += VARCHAR_LENGTH_SIZE;
        }
        // Next we copy bytes equal to the size of the field and increase our offsets
        memcpy((char*) data + data_offset, start + rec_offset, fieldSize);
        rec_offset += fieldSize;
        data_offset += fieldSize;
    }
}

//Given a record descriptor, delete the record identified by the given
//rid. Also, each time when a record is deleted, you will need to
//compact that page. That is, keep the free space in the middle of the
//page -- the slot table will be at one end, the record data area will
//be at the other end, and the free space should be in the middle.
RC RecordBasedFileManager::deleteRecord(FileHandle &fileHandle,
   const vector<Attribute> &recordDescriptor, const RID &rid){

   SlotDirectoryHeader slotHeader;
   SlotDirectoryRecordEntry dirRecEnt;

   // Retrieve the specific page
   void * pageData = malloc(PAGE_SIZE);
   if (fileHandle.readPage(rid.pageNum, pageData)){
      free(pageData);
      return RBFM_READ_FAILED;
   }

   // Checks if the specific slot id exists in the page
   slotHeader = getSlotDirectoryHeader(pageData);
   if(slotHeader.recordEntriesNumber < rid.slotNum){
      free(pageData);
      return RBFM_SLOT_DN_EXIST;
   }

   //Read in the correct directory record entry
   dirRecEnt = getSlotDirectoryRecordEntry(pageData, rid.slotNum);

   //A negative offset implies forwarding address for the record
   if(dirRecEnt.offset < 0){
      //Create a new temporary RID to look at the forwarding address
      //The page number is in the offset spot and the slot number is in
      //the length spot
      RID temp;
      temp.pageNum = -dirRecEnt.offset;
      temp.slotNum = dirRecEnt.length;
      //Once the new RID is set up, call delete again using new RID
      free(pageData);
      return(deleteRecord(fileHandle,recordDescriptor, temp));
   }

   //Otherwise, the record is on this page and can be deleted
   //Clear out memory spot
   memset (((char *) pageData + PAGE_SIZE - dirRecEnt.offset), 0,
      dirRecEnt.length);

   //Move each record after the deleted slot to its spot
   moveFollowingRecords(pageData, rid.slotNum + 1,
      dirRecEnt.offset - dirRecEnt.length);

   //Zero out end part of redundant data
   memset(((char *) pageData + PAGE_SIZE - slotHeader.freeSpaceOffset),
      0, dirRecEnt.length);

   //Set length of deleted slot to zero
   dirRecEnt = getSlotDirectoryRecordEntry(pageData, rid.slotNum);
   dirRecEnt.length = 0;
   setSlotDirectoryRecordEntry(pageData, rid.slotNum, dirRecEnt);



   free(pageData);
   return(0);
}

//Given a record descriptor, update the record identified by the given
//rid with the passed data. If the record grows and there is no space
//in the page to store the record, the record must be migrated to a new
//page with enough free space. Since you will soon be implementing an
//index structure, assume that records are identified by their RID
//values and when they migrate, you should leave a tombstone behind
//(pointing to the new location of the record). Also, each time when a
//record is updated to become smaller, you need to compact that page.
//That is, keep the free space in the middle of the page -- the slot
//table will be at one end, the record data area will be at the other
//end, and the free space should be in the middle. Again, the structure
//for *data is the same as the one we use for the insertRecord().
RC RecordBasedFileManager::updateRecord(FileHandle &fileHandle,
   const vector<Attribute> &recordDescriptor, const void *data,
   const RID &rid){

   SlotDirectoryHeader slotHeader;
   SlotDirectoryRecordEntry dirRecEnt;
   size_t newRecSize;
   int retVal;

   // Retrieve the specific page.  page data is the specific page size buffer.
   void * pageData = malloc(PAGE_SIZE);
   if (fileHandle.readPage(rid.pageNum, pageData)){
      free(pageData);
      return RBFM_READ_FAILED;
   }

   // Checks if the specific slot id exists in the page
   slotHeader = getSlotDirectoryHeader(pageData);
   if(slotHeader.recordEntriesNumber < rid.slotNum){
      free(pageData);
      return RBFM_SLOT_DN_EXIST;
   }

   //Read in the correct directory record entry
   dirRecEnt = getSlotDirectoryRecordEntry(pageData, rid.slotNum);

   //A negative offset implies forwarding address for the record
   if(dirRecEnt.offset < 0){
      //Create a new temporary RID to look at the forwarding address
      //The page number is in the offset spot and the slot number is in
      //the length spot
      RID temp;
      temp.pageNum = -dirRecEnt.offset;
      temp.slotNum = dirRecEnt.length;
      //Once the new RID is set up, call update again using new RID
      free(pageData);
      return(updateRecord(fileHandle,recordDescriptor, data, temp));
   }

   //Calculate how big the new record is going to be
   newRecSize = getRecordSize(recordDescriptor, data);

   //If they are equal then hooray! Write over it
   if(newRecSize == dirRecEnt.length){
      setRecordAtOffset(pageData, dirRecEnt.offset, recordDescriptor,
         data);
   }
   //The new record still fits in space but needs to move
   else if(newRecSize < dirRecEnt.length){
      //Clear out memory spot
      memset (((char *) pageData + PAGE_SIZE - dirRecEnt.offset), 0,
         dirRecEnt.length);

      //Write new record shifted over by the difference between sizes
      setRecordAtOffset(pageData,
         dirRecEnt.offset - (dirRecEnt.length - newRecSize), recordDescriptor,
         data);

      //Move all of the following records
      moveFollowingRecords(pageData, rid.slotNum + 1,
         dirRecEnt.offset - (dirRecEnt.length - newRecSize));
   }
   //Else, new size is greater than old and need to check remaining space
   else{
      retVal = moveFollowingRecords(pageData, rid.slotNum + 1,
         dirRecEnt.offset + (newRecSize - dirRecEnt.length));

      //moveRecords returns a -1 when there is not enough space to move
      if(retVal != 0){
         RID temp;
         //Try and insert this record elsewhere
         retVal = insertRecord(fileHandle, recordDescriptor, data, temp);

         //Fail if cannot insert
         if(retVal != 0){
            return(-1);
         }

         //Otherwise update directory entry to have forwarding address
         dirRecEnt = getSlotDirectoryRecordEntry(pageData, rid.slotNum);
         dirRecEnt.offset = -temp.pageNum;
         dirRecEnt.length = temp.slotNum;

         //Move all following records over this one as it on diff page
         moveFollowingRecords(pageData, rid.slotNum + 1,
            dirRecEnt.offset - dirRecEnt.length);

         //Zero out end part of redundant data
         memset(((char *) pageData + PAGE_SIZE - slotHeader.freeSpaceOffset),
            0, dirRecEnt.length);
      }
      //Otherwise there is enough space so just write in record
      else{
         setRecordAtOffset(pageData, dirRecEnt.offset +
            (newRecSize - dirRecEnt.length),
            recordDescriptor, data);
      }
   }


   free(pageData);
   return(0);
}

//Given a record descriptor, read a specific attribute of a record
//identified by a given rid.
RC RecordBasedFileManager::readAttribute(FileHandle &fileHandle,
   const vector<Attribute> &recordDescriptor, const RID &rid,
   const string &attributeName, void *data){

   SlotDirectoryHeader slotHeader;
   SlotDirectoryRecordEntry dirRecEnt;
   void * entireRecord;
   int retVal;
   int i;
   size_t offset = 0, fieldLength;

   // Retrieve the specific page
   void * pageData = malloc(PAGE_SIZE);
   if (fileHandle.readPage(rid.pageNum, pageData)){
      free(pageData);
      return RBFM_READ_FAILED;
   }

   // Checks if the specific slot id exists in the page
   slotHeader = getSlotDirectoryHeader(pageData);
   if(slotHeader.recordEntriesNumber < rid.slotNum){
      free(pageData);
      return RBFM_SLOT_DN_EXIST;
   }

   //Read in the correct directory record entry
   dirRecEnt = getSlotDirectoryRecordEntry(pageData, rid.slotNum);

   //A negative offset implies forwarding address for the record
   if(dirRecEnt.offset < 0){
      //Create a new temporary RID to look at the forwarding address
      //The page number is in the offset spot and the slot number is in
      //the length spot
      RID temp;
      temp.pageNum = -dirRecEnt.offset;
      temp.slotNum = dirRecEnt.length;
      //Once the new RID is set up, call read again using new RID
      free(pageData);
      return(readAttribute(fileHandle, recordDescriptor, temp,
         attributeName, data));
   }

   //Otherwise the record is on the page, so get it
   entireRecord = calloc(dirRecEnt.length, 1); //calloc is like a malloc and memset, so alocating memory with zeros.
   retVal = readRecord(fileHandle, recordDescriptor, rid, entireRecord);
   if(retVal != 0){
      free(entireRecord);
      free(pageData);
      return(-1);
   }

   //Find the offset in the data field
   offset += getNullIndicatorSize(recordDescriptor.size());

   for(i = 0; i < recordDescriptor.size(); i++){
      //First few bytes of dataToAdd is the NULL field so dataToAdd[i/8]
      //is the first byte for the first 8 fields, second byte for the
      //next few and so on. 0x80 = 0b1000 0000 so shifting that by i % 8
      //will shift the one up to 7 times and then restart. & with this
      //value will check to see if the corresponding NULL bit is set
      if(!(((char *)entireRecord)[i / 8] & (0x80 >> (i % 8)))){ //whole line corresponds to the bitwise math of checking if attribute is null.
         switch(recordDescriptor.at(i).type){
            case TypeInt:
            case TypeReal:
               fieldLength = sizeof(int); //Should be 4 bytes
               break;
            case TypeVarChar:
               fieldLength = *(int*)((char *)entireRecord + offset); //casting as char* to go byte by byte.  This is getting the size ofthe var char string.  Look at notes.
               fieldLength += sizeof(int);
               break;
         }
         //If this is the correct field
         if(recordDescriptor.at(i).name == attributeName){
            //Copy the bytes into data
            memcpy(data, ((char *)entireRecord+offset),fieldLength);
            //Breaking because we found it.
            break;
         }
         //Otherwise increment offset and keep going
         else{
         //Move on to the next attribute since we did not find it.
            offset += fieldLength;
         }
      }
   }

   //Free up pointers
   free(entireRecord);
   free(pageData);
   return(0);
}

//Given a record descriptor, scan a file, i.e., sequentially read all
//the entries in the file. A scan has a filter condition associated
//with it, e.g., it consists of a list of attributes to project out as
//well as a predicate on an attribute ("Sal > 40000"). Specifically,
//the parameter conditionAttribute here is the attribute's name that
//you are going to apply the filter on. The compOp parameter is the
//comparison type that is going to be used in the filtering process.
//The value parameter is the value of the conditionAttribute that is
//going to be used to filter out records. Note that the retrieved
//records should only have the fields that are listed in the vector
//attributeNames. Please take a look at the test cases for more
//information on how to use this method.
RC RecordBasedFileManager::scan(FileHandle &fileHandle,
    const vector<Attribute> &recordDescriptor,
    const string &conditionAttribute,
    const CompOp compOp,                  // comparision type such as "<" and "="
    const void *value,                    // used in the comparison
    const vector<string> &attributeNames, // a list of projected attributes
    RBFM_ScanIterator &rbfm_ScanIterator){

    RID current_RID;
    //create a variable index_pages that keeps track of the number of pages in index form
    int index_pages = fileHandle.getNumberOfPages() - 1;
    //allocate space for the buffer
    void * pageData = malloc(PAGE_SIZE);
    void * fieldData;
    int retVal;
    bool fitsCondition;
    int len;
    Attribute attr;
    FoundData data;
    vector<FoundData> dataList;

    //Find the correct field to compare to and alloc buffer for values
   for(auto a : recordDescriptor){
      if(a.name == conditionAttribute){
         attr = a;
         fieldData = malloc(a.length);
         break;
      }
   }
   //If the conditionAttribute is not the name of a field in the recDesc
   if(!fieldData){
      free(pageData);
      free(fieldData);
      return(-1);
   }


    //loop through all the pages of our file.
    for(int i = 0; i <= index_pages; i++){
        if (fileHandle.readPage(i, pageData)){
            free(pageData);
            free(fieldData);
            return RBFM_READ_FAILED;
        }

        //Start at the first page and first slot
        current_RID.pageNum = i;
        current_RID.slotNum = 0;

        while(rbfm_ScanIterator.getNextRecord(current_RID, pageData)
         != RBFM_EOF){

            fitsCondition = false;
           //Read in the attribute to compare
           retVal = readAttribute(fileHandle, recordDescriptor,
             current_RID, conditionAttribute, fieldData);
            if(retVal != 0){
               free(pageData);
               free(fieldData);
               return(retVal);
            }

            //Set the number of bytes to compare
            len = (attr.type != TypeVarChar) ? sizeof(int) :
               *(int *)fieldData;

            //Comparet the value to the field data depending on the op
            switch(compOp){
               case EQ_OP:
                  //=
                  if(attr.type == TypeVarChar){
                     string fieldString((char *) fieldData + 4, len);
                     string valueString((char *) value, len);
                     fitsCondition = (strcmp(fieldString.c_str(), valueString.c_str()) == 0)
                        ? true : false;
                  }else if(attr.type == TypeReal){
                     fitsCondition = (*(float *)fieldData == *(float *)value) ? true : false;
                  }else{
                     fitsCondition = (*(int *)fieldData == *(int *)value) ? true : false;
                  }
                  break;
               case LT_OP:
                  //<
                  if(attr.type == TypeVarChar){
                     string fieldString((char *) fieldData + 4, len);
                     string valueString((char *) value, len);
                     fitsCondition = (strcmp(fieldString.c_str(), valueString.c_str()) < 0)
                        ? true : false;
                  }else if(attr.type == TypeReal){
                     fitsCondition = (*(float *)fieldData < *(float *)value) ? true : false;
                  }else{
                     fitsCondition = (*(int *)fieldData < *(int *)value) ? true : false;
                  }
                  break;
               case LE_OP:
                  //<=
                  if(attr.type == TypeVarChar){
                     string fieldString((char *) fieldData + 4, len);
                     string valueString((char *) value, len);
                     fitsCondition = (strcmp(fieldString.c_str(), valueString.c_str()) <= 0)
                        ? true : false;
                  }else if(attr.type == TypeReal){
                     fitsCondition = (*(float *)fieldData <= *(float *)value) ? true : false;
                  }else{
                     fitsCondition = (*(int *)fieldData <= *(int *)value) ? true : false;
                  }
                  break;
               case GT_OP:
                  //>
                  if(attr.type == TypeVarChar){
                     string fieldString((char *) fieldData + 4, len);
                     string valueString((char *) value, len);
                     fitsCondition = (strcmp(fieldString.c_str(), valueString.c_str()) > 0)
                        ? true : false;
                  }else if(attr.type == TypeReal){
                     fitsCondition = (*(float *)fieldData > *(float *)value) ? true : false;
                  }else{
                     fitsCondition = (*(int *)fieldData > *(int *)value) ? true : false;
                  }
                  break;
               case GE_OP:
                  //>=
                  if(attr.type == TypeVarChar){
                     string fieldString((char *) fieldData + 4, len);
                     string valueString((char *) value, len);
                     fitsCondition = (strcmp(fieldString.c_str(), valueString.c_str()) >= 0)
                        ? true : false;
                  }else if(attr.type == TypeReal){
                     fitsCondition = (*(float *)fieldData >= *(float *)value) ? true : false;
                  }else{
                     fitsCondition = (*(int *)fieldData >= *(int *)value) ? true : false;
                  }
                  break;
               case NE_OP:
                  //!=
                  if(attr.type == TypeVarChar){
                     string fieldString((char *) fieldData + 4, len);
                     string valueString((char *) value, len);
                     fitsCondition = (strcmp(fieldString.c_str(), valueString.c_str()) != 0)
                        ? true : false;
                  }else if(attr.type == TypeReal){
                     fitsCondition = (*(float *)fieldData != *(float *)value) ? true : false;
                  }else{
                     fitsCondition = (*(int *)fieldData != *(int *)value) ? true : false;
                  }
                  break;
               case NO_OP:
                  fitsCondition = true;
                  //None
                  break;
            }

            //Page data no longer needed
            free(pageData);

            //If the record matches the criteria
            if(fitsCondition){
               //For each of the requested fields
               for(auto name : attributeNames){
                  //Find the matching attribute in the recordDescriptor
                  for(auto a : recordDescriptor){
                     if(a.name == name){
                        attr = a;
                        break;
                     }
                  }

                  //Create a buffer to read into
                  free(fieldData);
                  fieldData = malloc(attr.length);

                  //And read in the correct field
                  retVal = readAttribute(fileHandle, recordDescriptor,
                     current_RID, name, fieldData);
                  if(retVal != 0){
                     free(fieldData);
                     return(retVal);
                  }

                  //Fill in the field of data and add to list
                  data.fieldName = name;
                  data.data = (attr.type == TypeVarChar) ?
                     string(((char *) fieldData + sizeof(int)),
                     *(int *)fieldData) :
                     (attr.type == TypeReal) ?
                     to_string(*(float *)fieldData) :
                     to_string(*(int *)fieldData);
                  dataList.push_back(data);
               }
            }
            //Free if still initalized, should be but...
            if(fieldData){
               free(fieldData);
            }
        }
    }

    return(0);
}

//On page, move all of the records with slot number >= firstSlotToMove
//With the the record given by firstSlotToMove having endpoint at
//toOffset + 1
int RecordBasedFileManager::moveFollowingRecords(void *page,
   unsigned firstSlotToMove, unsigned toOffset){

   SlotDirectoryHeader slotHeader;
   SlotDirectoryRecordEntry dirRecEnt;
   int i;
   int offset;
   size_t totalSize = 0;

   //Gets th directory header
   slotHeader = getSlotDirectoryHeader(page);

   //Check if there is enough space to move all of the records
   for(i = firstSlotToMove; i < slotHeader.recordEntriesNumber; i++){
      //Read in the correct directory record entry
      dirRecEnt = getSlotDirectoryRecordEntry(page, i);

      //Total up sizes
      totalSize += dirRecEnt.length;
   }

   //getPageFreeSpaceSize will get amount of free space of unmodified
   //page while totalSize + toOffset will get the projected size since
   //sum would be the new offset and no new directory entries are being
   //added
   if(PAGE_SIZE - totalSize - toOffset < (sizeof(SlotDirectoryHeader) +
      slotHeader.recordEntriesNumber*sizeof(SlotDirectoryRecordEntry))){
      //There is a conflict when the final offset lies somewhere in the
      //directory at the beginning.
      return(-1);
   }

   //Set up the inital offset
   offset = toOffset;

   //For each record that comes after the one to delete
   for(i = firstSlotToMove; i < slotHeader.recordEntriesNumber; i++){
      //Read in the correct directory record entry
      dirRecEnt = getSlotDirectoryRecordEntry(page, i);

      //Increase the offset to write to by the length of the record
      offset += dirRecEnt.length;

      //And move the corresponding record
      //memmove(destPtr, srcPtr, numToMove), use becuase may overlap and
      //memcpy undefined for overlapping buffers
      memmove(((char *) page + PAGE_SIZE - offset),
         ((char *) page + PAGE_SIZE - dirRecEnt.offset),
         dirRecEnt.length);

      //Modify and write the new offset for the moved record
      dirRecEnt.offset = offset;
      setSlotDirectoryRecordEntry(page, i, dirRecEnt);
   }

   //Modify the record table header and copy in
   slotHeader.freeSpaceOffset = offset;
   setSlotDirectoryHeader(page, slotHeader);

   return(0);
}

RC RBFM_ScanIterator::getNextRecord(RID &rid, void *data){

   SlotDirectoryHeader *header;

   //Check for NULL pointer
   if(!data){
      return(RBFM_EOF);
   }

   //Grab the header of the current page
   header = (SlotDirectoryHeader *)((char *) data + PAGE_SIZE -
      2*sizeof(int));

   if(rid.slotNum + 1 < header->recordEntriesNumber){
      rid.slotNum++;
      return(0);
   } else{
      return(RBFM_EOF);
   }
}


RC RBFM_ScanIterator::close(){
   return(-1);
}
